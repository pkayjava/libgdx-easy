package com.badlogic.gdx.scenes.scene2d;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TransformDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Scaling;

/**
 * 
 * @author skhauv
 *
 */
public class Animation extends Widget {

	private Scaling scaling;
	private int align = Align.center;
	private float imageX, imageY, imageWidth, imageHeight;
	private com.badlogic.gdx.graphics.g2d.Animation<Drawable> animation = null;
	private Array<Drawable> items = null;
	private Drawable drawable;

	private float stateTime;

	private boolean flipX;
	private boolean flipY;

	public Animation(float speed, Array<Drawable> items) {
		this(speed, items, Scaling.stretch, Align.center);
	}

	public Animation(float speed, Array<Drawable> items, Scaling scaling) {
		this(speed, items, scaling, Align.center);
	}

	public Animation(float speed, Array<Drawable> items, Scaling scaling, int align) {
		this.scaling = scaling;
		this.align = align;
		this.items = items;
		this.animation = new com.badlogic.gdx.graphics.g2d.Animation<>(speed, this.items, PlayMode.LOOP);
		setDrawable(this.items.get(0));
		setSize(getPrefWidth(), getPrefHeight());
	}

	protected float getDrawableMinimumWidth() {
		float width = 0;
		for (Drawable drawable : this.items) {
			width = Math.max(width, drawable.getMinWidth());
		}
		return width;
	}

	protected float getDrawableMinimumHeight() {
		float height = 0;
		for (Drawable drawable : this.items) {
			height = Math.max(height, drawable.getMinHeight());
		}
		return height;
	}

	public void layout() {
		if (this.items == null)
			return;

		float regionWidth = getDrawableMinimumWidth();
		float regionHeight = getDrawableMinimumHeight();
		float width = getWidth();
		float height = getHeight();

		Vector2 size = scaling.apply(regionWidth, regionHeight, width, height);
		imageWidth = size.x;
		imageHeight = size.y;

		if ((align & Align.left) != 0)
			imageX = 0;
		else if ((align & Align.right) != 0)
			imageX = (int) (width - imageWidth);
		else
			imageX = (int) (width / 2 - imageWidth / 2);

		if ((align & Align.top) != 0)
			imageY = (int) (height - imageHeight);
		else if ((align & Align.bottom) != 0)
			imageY = 0;
		else
			imageY = (int) (height / 2 - imageHeight / 2);
	}

	public void draw(Batch batch, float parentAlpha) {
		validate();

		flip();

		Color color = getColor();
		batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);

		float x = getX();
		float y = getY();
		float scaleX = getScaleX();
		float scaleY = getScaleY();

		if (drawable instanceof TransformDrawable) {
			float rotation = getRotation();
			if (scaleX != 1 || scaleY != 1 || rotation != 0) {
				((TransformDrawable) drawable).draw(batch, x + imageX, y + imageY, getOriginX() - imageX,
						getOriginY() - imageY, imageWidth, imageHeight, scaleX, scaleY, rotation);
				return;
			}
		}
		if (drawable != null)
			drawable.draw(batch, x + imageX, y + imageY, imageWidth * scaleX, imageHeight * scaleY);
	}

	@Override
	public void act(float delta) {
		super.act(delta);
		this.stateTime = this.stateTime + delta;
		if (this.stateTime > this.animation.getAnimationDuration()) {
			this.stateTime = this.stateTime - this.animation.getAnimationDuration();
		}
		Drawable drawable = this.animation.getKeyFrame(this.stateTime);
		setDrawable(drawable);
	}

	/** @param drawable May be null. */
	public void setDrawable(Drawable drawable) {
		if (this.drawable == drawable)
			return;
		if (drawable != null) {
			if (getPrefWidth() != drawable.getMinWidth() || getPrefHeight() != drawable.getMinHeight())
				invalidateHierarchy();
		} else
			invalidateHierarchy();
		this.drawable = drawable;
	}

	/** @return May be null. */
	public Drawable getDrawable() {
		return drawable;
	}

	public void setScaling(Scaling scaling) {
		if (scaling == null)
			throw new IllegalArgumentException("scaling cannot be null.");
		this.scaling = scaling;
		invalidate();
	}

	public void setAlign(int align) {
		this.align = align;
		invalidate();
	}

	public float getMinWidth() {
		return 0;
	}

	public float getMinHeight() {
		return 0;
	}

	public float getPrefWidth() {
		if (this.animation != null) {
			return getDrawableMinimumWidth();
		}
		return 0;
	}

	public float getPrefHeight() {
		if (drawable != null) {
			return getDrawableMinimumHeight();
		}
		return 0;
	}

	public float getImageX() {
		return imageX;
	}

	public float getImageY() {
		return imageY;
	}

	public float getImageWidth() {
		return imageWidth;
	}

	public float getImageHeight() {
		return imageHeight;
	}

	public com.badlogic.gdx.graphics.g2d.Animation<Drawable> getAnimation() {
		return animation;
	}

	public void setFlip(boolean x, boolean y) {
		this.flipX = x;
		this.flipY = y;
	}

	protected void flip() {
		Drawable drawable = getDrawable();
		if (drawable instanceof SpriteDrawable) {
			((SpriteDrawable) drawable).getSprite().setFlip(this.flipX, this.flipY);
		}
	}

}
