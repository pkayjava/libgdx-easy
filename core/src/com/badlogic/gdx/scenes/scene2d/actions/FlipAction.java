package com.badlogic.gdx.scenes.scene2d.actions;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Animation;

public class FlipAction extends Action {

	private boolean flipX;

	private boolean flipY;

	public boolean act(float delta) {
		if (this.target instanceof Animation) {
			((Animation) this.target).setFlip(this.flipX, this.flipY);
		}
		return true;
	}

	public void flip(boolean flipX, boolean flipY) {
		this.flipX = flipX;
		this.flipY = flipY;
	}

}
