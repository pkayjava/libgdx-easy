package com.angkorteam.libgdx.easy;

import java.io.IOException;
import java.io.InputStream;

import org.mariuszgromada.math.mxparser.Argument;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.angkorteam.libgdx.easy.factory.ArgumentFactory;
import com.angkorteam.libgdx.easy.factory.StageFactory;
import com.angkorteam.libgdx.easy.factory.TextureAtlasFactory;
import com.angkorteam.libgdx.easy.factory.TextureFactory;
import com.angkorteam.libgdx.easy.factory.XMLFactory;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public abstract class Screen extends ScreenAdapter {

	private static final String TAG = Screen.class.getSimpleName();

	protected Game game;

	protected Array<Object> object;

	protected Array<Argument> argument;

	protected InputMultiplexer multiplexer;

	protected ArrayMap<String, Object> registry;

	protected Screen(Game game) {
		this.game = game;
		this.object = new Array<Object>();
		this.registry = new ArrayMap<>();
		this.argument = new Array<Argument>(true, 16, Argument.class);
		this.registry.putAll(this.game.getRegistry());
	}

	@Override
	public void show() {
		this.multiplexer = new InputMultiplexer();
		Gdx.input.setInputProcessor(this.multiplexer);
		String clazz = getClass().getSimpleName();
		FileHandle handle = Gdx.files.internal("xml/" + clazz + ".xml");

		try (InputStream inputStream = handle.read()) {
			Document document = XMLFactory.BUILDER.parse(inputStream);
			Element element = document.getDocumentElement();
			String tagName = element.getTagName();
			if (XMLFactory.ELEMENT_SCREEN.equals(tagName)) {
				NodeList nodes = element.getChildNodes();
				for (int i = 0; i < nodes.getLength(); i++) {
					if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
						Element e = (Element) nodes.item(i);
						if (XMLFactory.ELEMENT_TEXTURE.equals(e.getTagName())) {
							TextureFactory.create(this.registry, e);
						} else if (XMLFactory.ELEMENT_STAGE.equals(e.getTagName())) {
							Stage stage = StageFactory.create(this.argument, this.registry, e);
							this.object.add(stage);
							this.multiplexer.addProcessor(stage);
						} else if (XMLFactory.ELEMENT_TEXTURE_ATLAS.equals(e.getTagName())) {
							TextureAtlasFactory.create(this.registry, e);
						} else if (XMLFactory.ELEMENT_ARGUMENT.equals(e.getTagName())) {
							this.argument.add(ArgumentFactory.create(this.registry, e));
						} else {
							throw new GdxRuntimeException(String.format("unknown %s", e.getTagName()));
						}
					}
				}
			} else {
				throw new GdxRuntimeException(String.format("unknown %s", tagName));
			}
		} catch (IOException | SAXException e) {
			throw new GdxRuntimeException(e);
		}
	}

	@Override
	public void hide() {
	}

	@Override
	public void resize(int width, int height) {
		for (Object object : this.object) {
			if (object instanceof Stage) {
				((Stage) object).getViewport().update(width, height);
			}
		}
	}

	@Override
	public void render(float delta) {
		for (Object object : this.object) {
			if (object instanceof Stage) {
				((Stage) object).act(delta);
				((Stage) object).draw();
			} else if (object instanceof World) {

			}
		}
	}

}
