package com.angkorteam.libgdx.easy.validation;

import com.badlogic.gdx.math.Interpolation;

public class InterpolationValidator {

	public static Interpolation parse(String interpolation) {
		if ("bounce".equalsIgnoreCase(interpolation)) {
			return Interpolation.bounce;
		} else if ("bounceIn".equalsIgnoreCase(interpolation)) {
			return Interpolation.bounceIn;
		} else if ("bounceOut".equalsIgnoreCase(interpolation)) {
			return Interpolation.bounceOut;
		} else if ("circle".equalsIgnoreCase(interpolation)) {
			return Interpolation.circle;
		} else if ("circleIn".equalsIgnoreCase(interpolation)) {
			return Interpolation.circleIn;
		} else if ("circleOut".equalsIgnoreCase(interpolation)) {
			return Interpolation.circleOut;
		} else if ("elastic".equalsIgnoreCase(interpolation)) {
			return Interpolation.elastic;
		} else if ("elasticIn".equalsIgnoreCase(interpolation)) {
			return Interpolation.elasticIn;
		} else if ("elasticOut".equalsIgnoreCase(interpolation)) {
			return Interpolation.elasticOut;
		} else if ("exp10".equalsIgnoreCase(interpolation)) {
			return Interpolation.exp10;
		} else if ("exp10In".equalsIgnoreCase(interpolation)) {
			return Interpolation.exp10In;
		} else if ("exp10Out".equalsIgnoreCase(interpolation)) {
			return Interpolation.exp10Out;
		} else if ("exp5".equalsIgnoreCase(interpolation)) {
			return Interpolation.exp5;
		} else if ("exp5In".equalsIgnoreCase(interpolation)) {
			return Interpolation.exp5In;
		} else if ("exp5Out".equalsIgnoreCase(interpolation)) {
			return Interpolation.exp5Out;
		} else if ("fade".equalsIgnoreCase(interpolation)) {
			return Interpolation.fade;
		} else if ("linear".equalsIgnoreCase(interpolation)) {
			return Interpolation.linear;
		} else if ("pow2".equalsIgnoreCase(interpolation)) {
			return Interpolation.pow2;
		} else if ("pow2In".equalsIgnoreCase(interpolation)) {
			return Interpolation.pow2In;
		} else if ("pow2InInverse".equalsIgnoreCase(interpolation)) {
			return Interpolation.pow2InInverse;
		} else if ("pow2Out".equalsIgnoreCase(interpolation)) {
			return Interpolation.pow2Out;
		} else if ("pow2OutInverse".equalsIgnoreCase(interpolation)) {
			return Interpolation.pow2OutInverse;
		} else if ("pow3".equalsIgnoreCase(interpolation)) {
			return Interpolation.pow3;
		} else if ("pow3In".equalsIgnoreCase(interpolation)) {
			return Interpolation.pow3In;
		} else if ("pow3InInverse".equalsIgnoreCase(interpolation)) {
			return Interpolation.pow3InInverse;
		} else if ("pow3Out".equalsIgnoreCase(interpolation)) {
			return Interpolation.pow3Out;
		} else if ("pow3OutInverse".equalsIgnoreCase(interpolation)) {
			return Interpolation.pow3OutInverse;
		} else if ("pow4".equalsIgnoreCase(interpolation)) {
			return Interpolation.pow4;
		} else if ("pow4In".equalsIgnoreCase(interpolation)) {
			return Interpolation.pow4In;
		} else if ("pow4Out".equalsIgnoreCase(interpolation)) {
			return Interpolation.pow4Out;
		} else if ("pow5".equalsIgnoreCase(interpolation)) {
			return Interpolation.pow5;
		} else if ("pow5In".equalsIgnoreCase(interpolation)) {
			return Interpolation.pow5In;
		} else if ("pow5Out".equalsIgnoreCase(interpolation)) {
			return Interpolation.pow5Out;
		} else if ("sine".equalsIgnoreCase(interpolation)) {
			return Interpolation.sine;
		} else if ("sineIn".equalsIgnoreCase(interpolation)) {
			return Interpolation.sineIn;
		} else if ("sineOut".equalsIgnoreCase(interpolation)) {
			return Interpolation.sineOut;
		} else if ("smooth".equalsIgnoreCase(interpolation)) {
			return Interpolation.smooth;
		} else if ("smooth2".equalsIgnoreCase(interpolation)) {
			return Interpolation.smooth2;
		} else if ("smoother".equalsIgnoreCase(interpolation)) {
			return Interpolation.smoother;
		} else if ("swing".equalsIgnoreCase(interpolation)) {
			return Interpolation.swing;
		} else if ("swingIn".equalsIgnoreCase(interpolation)) {
			return Interpolation.swingIn;
		} else if ("swingOut".equalsIgnoreCase(interpolation)) {
			return Interpolation.swingOut;
		} else {
			throw new IllegalArgumentException(String.format("invalid interpolation %s", interpolation));
		}
	}

}
