package com.angkorteam.libgdx.easy.validation;

import com.badlogic.gdx.utils.Align;

public class AlignmentValidator {
	

//    1 - Align.center
//    3 - Align.center | Align.top
//    5 - Align.center | Align.bottom
//    9 - Align.center | Align.left
//    17 - Align.center | Align.right
//    10 - Align.top | Align.left
//    18 - Align.top | Align.right
//    12 - Align.bottom | Align.left
//    20 - Align.bottom | Align.right


	public static int parse(int alignment) {
		if (alignment == Align.center 
				|| alignment == (Align.center | Align.top)
				|| alignment == (Align.center | Align.bottom) 
				|| alignment == (Align.center | Align.left)
				|| alignment == (Align.center | Align.right) 
				|| alignment == (Align.top | Align.left)
				|| alignment == (Align.top | Align.right) 
				|| alignment == (Align.bottom | Align.left)
				|| alignment == (Align.bottom | Align.right)) {
			return alignment;
		} else {
			throw new IllegalArgumentException(String.format("invalid alignment %s", alignment));
		}
	}
}
