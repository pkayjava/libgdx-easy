package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class SelectBoxStyleFactory {

	public static SelectBox.SelectBoxStyle create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (SelectBox.SelectBoxStyle) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasFont = false;
		BitmapFont font = null;

		boolean hasFontColor = false;
		Color fontColor = null;

		boolean hasBackground = false;
		Drawable background = null;

		boolean hasScrollStyle = false;
		ScrollPane.ScrollPaneStyle scrollStyle = null;

		boolean hasListStyle = false;
		List.ListStyle listStyle = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_FONT.equals(e.getTagName())) {
					hasFont = true;
					font = FontFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_FONT_COLOR.equals(e.getTagName())) {
					hasFontColor = true;
					fontColor = ColorFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_BACKGROUND.equals(e.getTagName())) {
					hasBackground = true;
					background = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_SCROLL_STYLE.equals(e.getTagName())) {
					hasScrollStyle = true;
					scrollStyle = ScrollPaneStyleFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_LIST_STYLE.equals(e.getTagName())) {
					hasListStyle = true;
					listStyle = ListStyleFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();
		SelectBox.SelectBoxStyle object = null;
		if (hasFont && hasFontColor && hasBackground && hasScrollStyle && hasListStyle) {
			object = new SelectBox.SelectBoxStyle(font, fontColor, background, scrollStyle, listStyle);
			memory.add(XMLFactory.ELEMENT_FONT);
			memory.add(XMLFactory.ELEMENT_FONT_COLOR);
			memory.add(XMLFactory.ELEMENT_BACKGROUND);
			memory.add(XMLFactory.ELEMENT_SCROLL_STYLE);
			memory.add(XMLFactory.ELEMENT_LIST_STYLE);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else if (XMLFactory.ELEMENT_DISABLED_FONT_COLOR.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.disabledFontColor = ColorFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_BACKGROUND_OVER.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.backgroundOver = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_BACKGROUND_OPEN.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.backgroundOpen = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_BACKGROUND_DISABLED.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.backgroundDisabled = DrawableFactory.create(registry, e);
				} else {
					throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
