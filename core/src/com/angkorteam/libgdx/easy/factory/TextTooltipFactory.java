package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextTooltip;
import com.badlogic.gdx.scenes.scene2d.ui.TooltipManager;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class TextTooltipFactory {

	public static TextTooltip create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (TextTooltip) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasText = false;
		String text = null;

		boolean hasSkin = false;
		Skin skin = null;

		boolean hasStyleName = false;
		String styleName = null;

		boolean hasStyle = false;
		TextTooltip.TextTooltipStyle style = null;

		boolean hasManager = false;
		TooltipManager manager = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_TEXT.equals(e.getTagName())) {
					hasText = true;
					text = e.getTextContent().trim();
				} else if (XMLFactory.ELEMENT_MANAGER.equals(e.getTagName())) {
					hasManager = true;
					manager = TooltipManagerFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_STYLE_NAME.equals(e.getTagName())) {
					hasStyleName = true;
					styleName = e.getTextContent().trim();
				} else if (XMLFactory.ELEMENT_STYLE.equals(e.getTagName())) {
					hasStyle = true;
					style = TextTooltipStyleFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_SKIN.equals(e.getTagName())) {
					hasSkin = true;
					skin = SkinFactory.create(registry, e);
				}
			}
		}

		// constructor section

		Array<String> memory = new Array<String>();
		TextTooltip object = null;
		if (hasText && hasManager && hasStyle) {
			object = new TextTooltip(text, manager, style);
			memory.add(XMLFactory.ELEMENT_TEXT);
			memory.add(XMLFactory.ELEMENT_MANAGER);
			memory.add(XMLFactory.ELEMENT_STYLE);
		} else if (hasText && hasManager && hasSkin && hasStyleName) {
			object = new TextTooltip(text, manager, skin, styleName);
			memory.add(XMLFactory.ELEMENT_TEXT);
			memory.add(XMLFactory.ELEMENT_MANAGER);
			memory.add(XMLFactory.ELEMENT_SKIN);
			memory.add(XMLFactory.ELEMENT_STYLE_NAME);
		} else if (hasText && hasManager && hasSkin) {
			object = new TextTooltip(text, manager, skin);
			memory.add(XMLFactory.ELEMENT_TEXT);
			memory.add(XMLFactory.ELEMENT_MANAGER);
			memory.add(XMLFactory.ELEMENT_SKIN);
		} else if (hasText && hasStyle) {
			object = new TextTooltip(text, style);
			memory.add(XMLFactory.ELEMENT_TEXT);
			memory.add(XMLFactory.ELEMENT_STYLE);
		} else if (hasText && hasSkin && hasStyleName) {
			object = new TextTooltip(text, skin, styleName);
			memory.add(XMLFactory.ELEMENT_TEXT);
			memory.add(XMLFactory.ELEMENT_SKIN);
			memory.add(XMLFactory.ELEMENT_STYLE_NAME);
		} else if (hasText && hasSkin) {
			object = new TextTooltip(text, skin);
			memory.add(XMLFactory.ELEMENT_TEXT);
			memory.add(XMLFactory.ELEMENT_SKIN);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
