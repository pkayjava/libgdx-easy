package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.angkorteam.libgdx.easy.validation.InterpolationValidator;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RotateByAction;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class RotateByActionFactory {

	public static RotateByAction create(ArrayMap<String, Object> registry, Element element) {
		Attr rotationAmount = element.getAttributeNode(XMLFactory.ATTRIBUTE_ROTATION_AMOUNT);
		Attr duration = element.getAttributeNode(XMLFactory.ATTRIBUTE_DURATION);
		Attr interpolation = element.getAttributeNode(XMLFactory.ATTRIBUTE_INTERPOLATION);
		if (rotationAmount != null && duration != null && interpolation != null) {
			return Actions.rotateBy(Float.valueOf(rotationAmount.getValue()), Float.valueOf(duration.getValue()),
					InterpolationValidator.parse(interpolation.getTextContent().trim()));
		} else if (rotationAmount != null && duration != null) {
			return Actions.rotateBy(Float.valueOf(rotationAmount.getValue()), Float.valueOf(duration.getValue()));
		} else if (rotationAmount != null) {
			return Actions.rotateBy(Float.valueOf(rotationAmount.getValue()));
		} else {
			throw new GdxRuntimeException(String.format("%s invalid", element.getTagName()));
		}
	}

}
