package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.angkorteam.libgdx.easy.validation.InterpolationValidator;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ColorAction;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class ColorActionFactory {

	public static ColorAction create(ArrayMap<String, Object> registry, Element element) {
		Attr duration = element.getAttributeNode(XMLFactory.ATTRIBUTE_DURATION);
		Attr interpolation = element.getAttributeNode(XMLFactory.ATTRIBUTE_INTERPOLATION);

		boolean hasColor = false;
		Color color = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_COLOR.equals(e.getTagName())) {
					hasColor = true;
					color = ColorFactory.create(registry, e);
				}
			}
		}

		if (hasColor && duration != null && interpolation != null) {
			return Actions.color(color, Float.valueOf(duration.getValue()),
					InterpolationValidator.parse(interpolation.getTextContent().trim()));
		} else if (hasColor && duration != null) {
			return Actions.color(color, Float.valueOf(duration.getValue()));
		} else if (hasColor) {
			return Actions.color(color);
		} else {
			throw new GdxRuntimeException(String.format("%s invalid", element.getTagName()));
		}
	}

}
