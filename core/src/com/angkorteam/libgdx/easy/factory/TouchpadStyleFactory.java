package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class TouchpadStyleFactory {

	public static Touchpad.TouchpadStyle create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Touchpad.TouchpadStyle) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasBackground = false;
		Drawable background = null;

		boolean hasKnob = false;
		Drawable knob = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_BACKGROUND.equals(e.getTagName())) {
					hasBackground = true;
					background = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_KNOB.equals(e.getTagName())) {
					hasKnob = true;
					knob = DrawableFactory.create(registry, e);
				}
			}
		}

		Touchpad.TouchpadStyle object = null;

		if (hasBackground && hasKnob) {
			object = new Touchpad.TouchpadStyle(background, knob);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
