package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextTooltip;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class TextTooltipStyleFactory {

	public static TextTooltip.TextTooltipStyle create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (TextTooltip.TextTooltipStyle) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasLabel = false;
		Label.LabelStyle label = null;

		boolean hasBackground = false;
		Drawable background = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_LABEL.equals(e.getTagName())) {
					hasLabel = true;
					label = LabelStyleFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_BACKGROUND.equals(e.getTagName())) {
					hasBackground = true;
					background = DrawableFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();
		TextTooltip.TextTooltipStyle object = null;
		if (hasBackground && hasLabel) {
			object = new TextTooltip.TextTooltipStyle(label, background);
			memory.add(XMLFactory.ELEMENT_LABEL);
			memory.add(XMLFactory.ELEMENT_BACKGROUND);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else if (XMLFactory.ELEMENT_WRAP_WIDTH.equals(e.getTagName())) {
					object.wrapWidth = Float.valueOf(e.getTextContent().trim());
					memory.add(e.getTagName());
				} else {
					throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
