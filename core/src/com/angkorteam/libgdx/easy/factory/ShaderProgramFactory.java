package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class ShaderProgramFactory {

	public static ShaderProgram create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (ShaderProgram) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasVertexShader = false;
		String vertexShader = null;

		boolean hasFragmentShader = false;
		String fragmentShader = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_VERTEX_SHADER.equals(e.getTagName())) {
					hasVertexShader = true;
					vertexShader = Gdx.files.internal(e.getTextContent().trim()).readString();
				} else if (XMLFactory.ELEMENT_FRAGMENT_SHADER.equals(e.getTagName())) {
					hasFragmentShader = true;
					fragmentShader = Gdx.files.internal(e.getTextContent().trim()).readString();
				}
			}
		}

		ShaderProgram object = null;

		if (hasVertexShader && hasFragmentShader) {
			object = new ShaderProgram(vertexShader, fragmentShader);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
