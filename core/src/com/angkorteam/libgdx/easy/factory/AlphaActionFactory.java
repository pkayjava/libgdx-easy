package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.angkorteam.libgdx.easy.validation.InterpolationValidator;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class AlphaActionFactory {

	public static AlphaAction create(ArrayMap<String, Object> registry, Element element) {
		Attr duration = element.getAttributeNode(XMLFactory.ATTRIBUTE_DURATION);
		Attr interpolation = element.getAttributeNode(XMLFactory.ATTRIBUTE_INTERPOLATION);
		Attr alpha = element.getAttributeNode(XMLFactory.ATTRIBUTE_ALPHA);

		if (alpha != null && duration != null && interpolation != null) {
			return Actions.alpha(Float.valueOf(alpha.getValue()), Float.valueOf(duration.getValue()),
					InterpolationValidator.parse(interpolation.getTextContent().trim()));
		} else if (alpha != null && duration != null) {
			return Actions.alpha(Float.valueOf(alpha.getValue()), Float.valueOf(duration.getValue()));
		} else if (alpha != null) {
			return Actions.alpha(Float.valueOf(alpha.getValue()));
		} else {
			throw new GdxRuntimeException(String.format("%s invalid", element.getTagName()));
		}
	}

}
