package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.SplitPane;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class SplitPaneFactory {

	public static SplitPane create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (SplitPane) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasFirstWidget = false;
		Actor firstWidget = null;

		boolean hasSecondWidget = false;
		Actor secondWidget = null;

		boolean hasVertical = false;
		boolean vertical = false;

		boolean hasSkin = false;
		Skin skin = null;

		boolean hasStyleName = false;
		String styleName = null;

		boolean hasStyle = false;
		SplitPane.SplitPaneStyle style = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_FIRST_WIDGET.equals(e.getTagName())) {
					hasFirstWidget = true;
					firstWidget = ActorFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_SECOND_WIDGET.equals(e.getTagName())) {
					hasSecondWidget = true;
					secondWidget = ActorFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_VERTICAL.equals(e.getTagName())) {
					hasVertical = true;
					vertical = Boolean.valueOf(e.getTextContent().trim());
				} else if (XMLFactory.ELEMENT_SKIN.equals(e.getTagName())) {
					hasSkin = true;
					skin = SkinFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_STYLE_NAME.equals(e.getTagName())) {
					hasStyleName = true;
					styleName = e.getTextContent().trim();
				} else if (XMLFactory.ELEMENT_STYLE.equals(e.getTagName())) {
					hasStyle = true;
					style = SplitPaneStyleFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();
		SplitPane object = null;
		if (hasFirstWidget && hasSecondWidget && hasVertical && hasSkin && hasStyleName) {
			object = new SplitPane(firstWidget, secondWidget, vertical, skin, styleName);
			memory.add(XMLFactory.ELEMENT_FIRST_WIDGET);
			memory.add(XMLFactory.ELEMENT_SECOND_WIDGET);
			memory.add(XMLFactory.ELEMENT_VERTICAL);
			memory.add(XMLFactory.ELEMENT_SKIN);
			memory.add(XMLFactory.ELEMENT_STYLE_NAME);
		} else if (hasFirstWidget && hasSecondWidget && hasVertical && hasSkin) {
			object = new SplitPane(firstWidget, secondWidget, vertical, skin);
			memory.add(XMLFactory.ELEMENT_FIRST_WIDGET);
			memory.add(XMLFactory.ELEMENT_SECOND_WIDGET);
			memory.add(XMLFactory.ELEMENT_VERTICAL);
			memory.add(XMLFactory.ELEMENT_SKIN);
		} else if (hasFirstWidget && hasSecondWidget && hasVertical && hasStyle) {
			object = new SplitPane(firstWidget, secondWidget, vertical, style);
			memory.add(XMLFactory.ELEMENT_FIRST_WIDGET);
			memory.add(XMLFactory.ELEMENT_SECOND_WIDGET);
			memory.add(XMLFactory.ELEMENT_VERTICAL);
			memory.add(XMLFactory.ELEMENT_STYLE);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else {
					throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
