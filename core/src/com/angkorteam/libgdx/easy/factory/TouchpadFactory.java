package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class TouchpadFactory {

	public static Touchpad create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Touchpad) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasDeadzoneRadius = false;
		float deadzoneRadius = -1;

		boolean hasSkin = false;
		Skin skin = null;

		boolean hasStyleName = false;
		String styleName = null;

		boolean hasStyle = false;
		Touchpad.TouchpadStyle style = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_DEAD_ZONE_RADIUS.equals(e.getTagName())) {
					hasDeadzoneRadius = true;
					deadzoneRadius = Float.valueOf(e.getTextContent().trim());
				} else if (XMLFactory.ELEMENT_STYLE_NAME.equals(e.getTagName())) {
					hasStyleName = true;
					styleName = e.getTextContent().trim();
				} else if (XMLFactory.ELEMENT_STYLE.equals(e.getTagName())) {
					hasStyle = true;
					style = TouchpadStyleFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_SKIN.equals(e.getTagName())) {
					hasSkin = true;
					skin = SkinFactory.create(registry, e);
				}
			}
		}

		Touchpad object = null;

		if (hasDeadzoneRadius && hasStyleName && hasSkin) {
			object = new Touchpad(deadzoneRadius, skin, styleName);
		} else if (hasDeadzoneRadius && hasSkin) {
			object = new Touchpad(deadzoneRadius, skin);
		} else if (hasDeadzoneRadius && hasStyle) {
			object = new Touchpad(deadzoneRadius, style);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
