package com.angkorteam.libgdx.easy.factory;

import org.mariuszgromada.math.mxparser.Argument;
import org.mariuszgromada.math.mxparser.Expression;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class ArgumentFactory {

	public static Argument create(ArrayMap<String, Object> registry, Element element) {
		Attr name = element.getAttributeNode(XMLFactory.ATTRIBUTE_NAME);
		Attr value = element.getAttributeNode(XMLFactory.ATTRIBUTE_VALUE);
		if (name != null && value != null) {
			return new Argument(name.getValue(), new Expression(value.getValue()).calculate());
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", element.getTagName()));
		}
	}

}
