package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class SkinFactory {

	public static Skin create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Skin) XMLFactory.findRef(registry, element, ref);
		}

		Skin object = new Skin();

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				String name = e.getAttribute(XMLFactory.ATTRIBUTE_NAME);
				if (name == null || "".equals(name)) {
					name = XMLFactory.ATTRIBUTE_NAME_DEFAULT;
				}
				if (XMLFactory.ELEMENT_LABEL.equals(e.getTagName())) {
					object.add(name, LabelStyleFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_TEXT_FIELD.equals(e.getTagName())) {
					object.add(name, TextFieldStyleFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_FONT.equals(e.getTagName())) {
					object.add(name, FontFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_BUTTON.equals(e.getTagName())) {
					object.add(name, ButtonStyleFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_TEXT_BUTTON.equals(e.getTagName())) {
					object.add(name, TextButtonStyleFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_IMAGE_BUTTON.equals(e.getTagName())) {
					object.add(name, ImageButtonStyleFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_IMAGE_TEXT_BUTTON.equals(e.getTagName())) {
					object.add(name, ImageTextButtonStyleFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_CHECK_BOX.equals(e.getTagName())) {
					object.add(name, CheckBoxStyleFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_LIST.equals(e.getTagName())) {
					object.add(name, ListStyleFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_SELECT_BOX.equals(e.getTagName())) {
					object.add(name, ListStyleFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_SCROLL_PANE.equals(e.getTagName())) {
					object.add(name, ListStyleFactory.create(registry, e));
				} else {
					throw new GdxRuntimeException(String.format("unknown style %s", e.getTagName()));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;

	}
}
