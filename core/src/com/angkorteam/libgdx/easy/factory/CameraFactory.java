package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class CameraFactory {

	public static Camera create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Camera) XMLFactory.findRef(registry, element, ref);
		}

		// constructor section

		Camera object = null;

		String type = element.getAttribute(XMLFactory.ATTRIBUTE_TYPE);
		if (XMLFactory.ATTRIBUTE_TYPE_PERSPECTIVE.equals(type)) {
			object = createPerspectiveCamera(element);
		} else if (XMLFactory.ATTRIBUTE_TYPE_ORTHOGRAPHIC.equals(type)) {
			object = createOrthographicCamera(element);
		} else {
			throw new GdxRuntimeException(String.format("wrong type %s", type));
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

	private static Camera createPerspectiveCamera(Element element) {

		// constructor section

		boolean hasFieldOfViewY = false;
		int fieldOfViewY = -1;

		boolean hasViewportWidth = false;
		int viewportWidth = -1;

		boolean hasViewportHeight = false;
		int viewportHeight = -1;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_FIELD_OF_VIEW_Y.equals(e.getTagName())) {
					fieldOfViewY = Integer.valueOf(e.getTextContent());
					hasFieldOfViewY = true;
				} else if (XMLFactory.ELEMENT_VIEWPORT_WIDTH.equals(e.getTagName())) {
					viewportWidth = Integer.valueOf(e.getTextContent());
					hasViewportWidth = true;
				} else if (XMLFactory.ELEMENT_VIEWPORT_HEIGHT.equals(e.getTagName())) {
					viewportHeight = Integer.valueOf(e.getTextContent());
					hasViewportHeight = true;
				}
			}
		}
		if (hasViewportHeight && hasViewportWidth && hasFieldOfViewY) {
			return new PerspectiveCamera(fieldOfViewY, viewportWidth, viewportHeight);
		} else if (!hasViewportHeight && !hasViewportWidth && !hasFieldOfViewY) {
			return new PerspectiveCamera();
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", element.getTagName()));
		}
	}

	private static Camera createOrthographicCamera(Element element) {

		// constructor section
		boolean hasViewportWidth = false;
		int viewportWidth = -1;

		boolean hasViewportHeight = false;
		int viewportHeight = -1;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_VIEWPORT_WIDTH.equals(e.getTagName())) {
					viewportWidth = Integer.valueOf(e.getTextContent());
					hasViewportWidth = true;
				} else if (XMLFactory.ELEMENT_VIEWPORT_HEIGHT.equals(e.getTagName())) {
					viewportHeight = Integer.valueOf(e.getTextContent());
					hasViewportHeight = true;
				}
			}
		}
		if (hasViewportHeight && hasViewportWidth) {
			return new OrthographicCamera(viewportWidth, viewportHeight);
		} else if (!hasViewportHeight && !hasViewportWidth) {
			return new OrthographicCamera();
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", element.getTagName()));
		}
	}

}
