package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class SliderFactory {

	public static Slider create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Slider) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasMinimum = false;
		float minimum = -1;

		boolean hasMaximum = false;
		float maximum = -1;

		boolean hasStepSize = false;
		float stepSize = -1;

		boolean hasVertical = false;
		boolean vertical = false;

		boolean hasSkin = false;
		Skin skin = null;

		boolean hasStyleName = false;
		String styleName = null;

		boolean hasStyle = false;
		Slider.SliderStyle style = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_MINIMUM.equals(e.getTagName())) {
					hasMinimum = true;
					minimum = Float.valueOf(e.getTextContent().trim());
				} else if (XMLFactory.ELEMENT_MAXIMUM.equals(e.getTagName())) {
					hasMaximum = true;
					maximum = Float.valueOf(e.getTextContent().trim());
				} else if (XMLFactory.ELEMENT_STEP_SIZE.equals(e.getTagName())) {
					hasStepSize = true;
					stepSize = Float.valueOf(e.getTextContent().trim());
				} else if (XMLFactory.ELEMENT_VERTICAL.equals(e.getTagName())) {
					hasVertical = true;
					vertical = Boolean.valueOf(e.getTextContent().trim());
				} else if (XMLFactory.ELEMENT_STYLE_NAME.equals(e.getTagName())) {
					hasStyleName = true;
					styleName = e.getTextContent().trim();
				} else if (XMLFactory.ELEMENT_STYLE.equals(e.getTagName())) {
					hasStyle = true;
					style = SliderStyleFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_SKIN.equals(e.getTagName())) {
					hasSkin = true;
					skin = SkinFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();
		Slider object = null;
		if (hasMinimum && hasMaximum && hasStepSize && hasVertical && hasSkin && hasStyleName) {
			object = new Slider(minimum, maximum, stepSize, vertical, skin, styleName);
			memory.add(XMLFactory.ELEMENT_MINIMUM);
			memory.add(XMLFactory.ELEMENT_MAXIMUM);
			memory.add(XMLFactory.ELEMENT_STEP_SIZE);
			memory.add(XMLFactory.ELEMENT_VERTICAL);
			memory.add(XMLFactory.ELEMENT_SKIN);
			memory.add(XMLFactory.ELEMENT_STYLE_NAME);
		} else if (hasMinimum && hasMaximum && hasStepSize && hasVertical && hasSkin) {
			object = new Slider(minimum, maximum, stepSize, vertical, skin);
			memory.add(XMLFactory.ELEMENT_MINIMUM);
			memory.add(XMLFactory.ELEMENT_MAXIMUM);
			memory.add(XMLFactory.ELEMENT_STEP_SIZE);
			memory.add(XMLFactory.ELEMENT_VERTICAL);
			memory.add(XMLFactory.ELEMENT_SKIN);
		} else if (hasMinimum && hasMaximum && hasStepSize && hasVertical && hasStyle) {
			object = new Slider(minimum, maximum, stepSize, vertical, style);
			memory.add(XMLFactory.ELEMENT_MINIMUM);
			memory.add(XMLFactory.ELEMENT_MAXIMUM);
			memory.add(XMLFactory.ELEMENT_STEP_SIZE);
			memory.add(XMLFactory.ELEMENT_VERTICAL);
			memory.add(XMLFactory.ELEMENT_STYLE);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
