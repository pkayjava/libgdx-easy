package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class WidgetGroupFactory {

	public static WidgetGroup create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (WidgetGroup) XMLFactory.findRef(registry, element, ref);
		}

		// constructor section

		WidgetGroup object = new WidgetGroup();

		XMLFactory.register(registry, element, object);

		return object;
	}

}
