package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class TextAreaFactory {

	public static TextArea create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (TextArea) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasText = false;
		String text = null;

		boolean hasSkin = false;
		Skin skin = null;

		boolean hasStyleName = false;
		String styleName = null;

		boolean hasStyle = false;
		TextField.TextFieldStyle style = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_TEXT.equals(e.getTagName())) {
					hasText = true;
					text = e.getTextContent();
				} else if (XMLFactory.ELEMENT_SKIN.equals(e.getTagName())) {
					hasSkin = true;
					skin = SkinFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_STYLE_NAME.equals(e.getTagName())) {
					hasStyleName = true;
					styleName = e.getTextContent();
				} else if (XMLFactory.ELEMENT_STYLE.equals(e.getTagName())) {
					hasStyle = true;
					style = TextFieldStyleFactory.create(registry, e);
				}
			}
		}

		TextArea object = null;
		if (hasText && hasSkin && hasStyleName) {
			object = new TextArea(text, skin, styleName);
		} else if (hasText && hasSkin) {
			object = new TextArea(text, skin);
		} else if (hasText && hasStyle) {
			object = new TextArea(text, style);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_DISABLED) != null) {
			boolean disabled = Boolean.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_DISABLED).getValue());
			object.setDisabled(disabled);
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_PASSWORD_MODE) != null) {
			boolean passwordMode = Boolean
					.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_PASSWORD_MODE).getValue());
			if (passwordMode) {
				object.setPasswordMode(passwordMode);
				if (element.getAttributeNode(XMLFactory.ATTRIBUTE_PASSWORD_CHARACTER) != null) {
					String passwordCharacter = element.getAttributeNode(XMLFactory.ATTRIBUTE_PASSWORD_CHARACTER)
							.getValue();
					if (passwordCharacter == null || "".equals(passwordCharacter) || passwordCharacter.length() > 1) {
						passwordCharacter = "*";
					}
					object.setPasswordCharacter(passwordCharacter.toCharArray()[0]);
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
