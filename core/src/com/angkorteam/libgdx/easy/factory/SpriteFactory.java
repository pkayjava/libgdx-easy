package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class SpriteFactory {

	public static Sprite create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Sprite) XMLFactory.findRef(registry, element, ref);
		}

		// constructor section

		boolean hasRegion = false;
		TextureRegion region = null;

		boolean hasTexture = false;
		Texture texture = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_TEXTURE.equals(e.getTagName())) {
					hasTexture = true;
					texture = TextureFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_REGION.equals(e.getTagName())) {
					hasRegion = true;
					region = TextureRegionFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();

		Sprite object = null;
		if (hasTexture) {
			object = new Sprite(texture);
			memory.add(XMLFactory.ELEMENT_TEXTURE);
		} else if (hasRegion) {
			if (region instanceof Sprite) {
				object = new Sprite((Sprite) region);
			} else {
				object = new Sprite(region);
			}
			memory.add(XMLFactory.ELEMENT_REGION);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else if (XMLFactory.ELEMENT_FLIP.equals(e.getTagName())) {
					SpriteFactory.setFlip(e, object);
				} else {
					throw new GdxRuntimeException(String.format("tag %s invalid", tagName));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

	public static void setFlip(Element element, Sprite object) {
		Attr flipX = element.getAttributeNode(XMLFactory.ATTRIBUTE_FLIP_X);
		Attr flipY = element.getAttributeNode(XMLFactory.ATTRIBUTE_FLIP_Y);
		if (flipX != null && flipY != null) {
			object.setFlip(Boolean.valueOf(flipX.getValue()), Boolean.valueOf(flipY.getValue()));
		} else {
			throw new GdxRuntimeException(String.format("%s invalid", element.getTagName()));
		}
	}

}
