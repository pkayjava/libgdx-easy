package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Tooltip;
import com.badlogic.gdx.scenes.scene2d.ui.TooltipManager;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class TooltipFactory {

	public static Tooltip<Actor> create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Tooltip<Actor>) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasContent = false;
		Actor content = null;

		boolean hasManager = false;
		TooltipManager manager = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_CONTENT.equals(e.getTagName())) {
					hasContent = true;
					content = ActorFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_MANAGER.equals(e.getTagName())) {
					hasManager = true;
					manager = TooltipManagerFactory.create(registry, e);
				}
			}
		}

		// constructor section

		Tooltip<Actor> object = null;
		if (hasContent && hasManager) {
			object = new Tooltip<>(content, manager);
		} else if (hasContent) {
			object = new Tooltip<>(content);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
