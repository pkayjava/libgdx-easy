package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class WindowStyleFactory {

	public static Window.WindowStyle create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Window.WindowStyle) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasTitleFont = false;
		BitmapFont titleFont = null;

		boolean hasTitleFontColor = false;
		Color titleFontColor = null;

		boolean hasBackground = false;
		Drawable background = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_TITLE_FONT.equals(e.getTagName())) {
					hasTitleFont = true;
					titleFont = FontFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_BACKGROUND.equals(e.getTagName())) {
					hasBackground = true;
					background = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_TITLE_FONT_COLOR.equals(e.getTagName())) {
					hasTitleFontColor = true;
					titleFontColor = ColorFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();
		Window.WindowStyle object = null;
		if (hasBackground && hasTitleFontColor && hasTitleFont) {
			object = new Window.WindowStyle(titleFont, titleFontColor, background);
			memory.add(XMLFactory.ELEMENT_TITLE_FONT);
			memory.add(XMLFactory.ELEMENT_TITLE_FONT_COLOR);
			memory.add(XMLFactory.ELEMENT_BACKGROUND);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else if (XMLFactory.ELEMENT_STAGE_BACKGROUND.equals(e.getTagName())) {
					object.stageBackground = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else {
					throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
