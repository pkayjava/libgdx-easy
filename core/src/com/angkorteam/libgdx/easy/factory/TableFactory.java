package com.angkorteam.libgdx.easy.factory;

import org.mariuszgromada.math.mxparser.Argument;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class TableFactory {

	public static Table create(Array<Argument> argument, ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Table) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasSkin = false;
		Skin skin = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_SKIN.equals(e.getTagName())) {
					hasSkin = true;
					skin = SkinFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();
		Table object = null;
		if (hasSkin) {
			object = new Table(skin);
			memory.add(XMLFactory.ELEMENT_SKIN);
		} else {
			object = new Table();
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_DEBUG) != null) {
			boolean debug = Boolean.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_DEBUG).getValue());
			object.setDebug(debug);
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_FILL_PARENT) != null) {
			boolean fillParent = Boolean.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_FILL_PARENT).getValue());
			object.setFillParent(fillParent);
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_CENTER) != null) {
			boolean center = Boolean.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_CENTER).getValue());
			if (center) {
				object.center();
			}
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_BOTTOM) != null) {
			boolean bottom = Boolean.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_BOTTOM).getValue());
			if (bottom) {
				object.bottom();
			}
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_RIGHT) != null) {
			boolean right = Boolean.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_RIGHT).getValue());
			if (right) {
				object.right();
			}
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_LEFT) != null) {
			boolean left = Boolean.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_LEFT).getValue());
			if (left) {
				object.left();
			}
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_TOP) != null) {
			boolean top = Boolean.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_TOP).getValue());
			if (top) {
				object.top();
			}
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN) != null) {
			float margin = Float.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN).getValue());
			object.pad(margin);
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN_BOTTOM) != null) {
			float padBottom = Float.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN_BOTTOM).getValue());
			object.padBottom(padBottom);
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN_LEFT) != null) {
			float padLeft = Float.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN_LEFT).getValue());
			object.padLeft(padLeft);
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN_RIGHT) != null) {
			float padRight = Float.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN_RIGHT).getValue());
			object.padRight(padRight);
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN_TOP) != null) {
			float padTop = Float.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN_TOP).getValue());
			object.padTop(padTop);
		}

		boolean firstRow = true;

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else if (XMLFactory.ELEMENT_ROW.equals(e.getTagName())) {
					if (!firstRow) {
						object.row();
					}
					RowFactory.create(object, argument, registry, e);
					firstRow = false;
				} else if (XMLFactory.ELEMENT_BACKGROUND.equals(e.getTagName())) {
					object.setBackground(DrawableFactory.create(registry, e));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;

	}
}
