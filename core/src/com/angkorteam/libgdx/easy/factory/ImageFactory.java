package com.angkorteam.libgdx.easy.factory;

import org.mariuszgromada.math.mxparser.Argument;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.Scaling;

public class ImageFactory {

	public static Image create(Array<Argument> argument, ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Image) XMLFactory.findRef(registry, element, ref);
		}

		// constructor section

		boolean hasPatch = false;
		NinePatch patch = null;

		boolean hasRegion = false;
		TextureRegion region = null;

		boolean hasSprite = false;
		Sprite sprite = null;

		boolean hasTexture = false;
		Texture texture = null;

		boolean hasSkin = false;
		Skin skin = null;

		boolean hasDrawableName = false;
		String drawableName = null;

		boolean hasDrawable = false;
		Drawable drawable = null;

		boolean hasScaling = false;
		Scaling scaling = null;

		boolean hasAlign = false;
		int align = -1;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_TEXTURE.equals(e.getTagName())) {
					hasTexture = true;
					texture = TextureFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_SKIN.equals(e.getTagName())) {
					hasSkin = true;
					skin = SkinFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_DRAWABLE_NAME.equals(e.getTagName())) {
					hasDrawableName = true;
					drawableName = e.getTextContent().trim();
				} else if (XMLFactory.ELEMENT_DRAWABLE.equals(e.getTagName())) {
					hasDrawable = true;
					drawable = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_SCALING.equals(e.getTagName())) {
					hasScaling = true;
					scaling = Scaling.valueOf(e.getTextContent().trim());
				} else if (XMLFactory.ELEMENT_ALIGN.equals(e.getTagName())) {
					hasAlign = true;
					align = Integer.valueOf(e.getTextContent().trim());
				} else if (XMLFactory.ELEMENT_REGION.equals(e.getTagName())) {
					hasRegion = true;
					region = TextureRegionFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_SPRITE.equals(e.getTagName())) {
					hasSprite = true;
					sprite = SpriteFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_PATCH.equals(e.getTagName())) {
					hasPatch = true;
					NinePatchDrawable d = (NinePatchDrawable) DrawableFactory.create(registry, e);
					patch = d.getPatch();
				}
			}
		}

		Array<String> memory = new Array<String>();

		Image object = null;
		if (hasDrawable && hasScaling && hasAlign) {
			object = new Image(drawable, scaling, align);
			memory.add(XMLFactory.ELEMENT_DRAWABLE);
			memory.add(XMLFactory.ELEMENT_SCALING);
			memory.add(XMLFactory.ELEMENT_ALIGN);
		} else if (hasDrawable && hasScaling) {
			object = new Image(drawable, scaling);
			memory.add(XMLFactory.ELEMENT_DRAWABLE);
			memory.add(XMLFactory.ELEMENT_SCALING);
		} else if (hasDrawable) {
			object = new Image(drawable);
			memory.add(XMLFactory.ELEMENT_DRAWABLE);
		} else if (hasSkin && hasDrawableName) {
			object = new Image(skin, drawableName);
			memory.add(XMLFactory.ELEMENT_SKIN);
			memory.add(XMLFactory.ELEMENT_DRAWABLE_NAME);
		} else if (hasTexture) {
			object = new Image(new TextureRegion(texture));
			memory.add(XMLFactory.ELEMENT_TEXTURE);
		} else if (hasPatch) {
			object = new Image(patch);
			memory.add(XMLFactory.ELEMENT_PATCH);
		} else if (hasRegion) {
			object = new Image(new TextureRegionDrawable(region));
			memory.add(XMLFactory.ELEMENT_REGION);
		} else if (hasSprite) {
			object = new Image(new SpriteDrawable(sprite));
			memory.add(XMLFactory.ELEMENT_SPRITE);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else if (XMLFactory.ELEMENT_SCALING.equals(e.getTagName())) {
					memory.add(e.getTagName());
					scaling = Scaling.valueOf(e.getTextContent().trim());
					object.setScaling(scaling);
				} else if (XMLFactory.ELEMENT_POSITION.equals(e.getTagName())) {
					memory.add(e.getTagName());
					ActorFactory.setPosition(argument, e, object);
				} else if (XMLFactory.ELEMENT_SIZE.equals(e.getTagName())) {
					memory.add(e.getTagName());
					ActorFactory.setSize(e, object);
				} else if (XMLFactory.ELEMENT_COLOR.equals(e.getTagName())) {
					memory.add(e.getTagName());
					ActorFactory.setColor(registry, e, object);
				} else if (XMLFactory.ELEMENT_TIMELINE.equals(e.getTagName())) {
					memory.add(e.getTagName());
					ActorFactory.setAction(registry, e, object);
				} else if (XMLFactory.ELEMENT_SCALE.equals(e.getTagName())) {
					memory.add(e.getTagName());
					ActorFactory.setScale(e, object);
				} else {
					throw new GdxRuntimeException(String.format("tag %s invalid", tagName));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
