package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class TreeFactory {

	public static Tree create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Tree) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasSkin = false;
		Skin skin = null;

		boolean hasStyleName = false;
		String styleName = null;

		boolean hasStyle = false;
		Tree.TreeStyle style = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_SKIN.equals(e.getTagName())) {
					hasSkin = true;
					skin = SkinFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_STYLE_NAME.equals(e.getTagName())) {
					hasStyleName = true;
					styleName = e.getTextContent();
				} else if (XMLFactory.ELEMENT_STYLE.equals(e.getTagName())) {
					hasStyle = true;
					style = TreeStyleFactory.create(registry, e);
				}
			}
		}

		Tree object = null;
		if (hasSkin && hasStyleName) {
			object = new Tree(skin, styleName);
		} else if (hasSkin) {
			object = new Tree(skin);
		} else if (hasStyle) {
			object = new Tree(style);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
