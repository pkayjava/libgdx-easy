package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.angkorteam.libgdx.easy.validation.InterpolationValidator;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class FadeInActionFactory {

	public static AlphaAction create(ArrayMap<String, Object> registry, Element element) {
		Attr duration = element.getAttributeNode(XMLFactory.ATTRIBUTE_DURATION);
		Attr interpolation = element.getAttributeNode(XMLFactory.ATTRIBUTE_INTERPOLATION);
		if (duration != null && interpolation != null) {
			return Actions.fadeIn(Float.valueOf(duration.getValue()),
					InterpolationValidator.parse(interpolation.getTextContent().trim()));
		} else if (duration != null) {
			return Actions.fadeIn(Float.valueOf(duration.getValue()));
		} else {
			throw new GdxRuntimeException(String.format("%s invalid", element.getTagName()));
		}
	}

}
