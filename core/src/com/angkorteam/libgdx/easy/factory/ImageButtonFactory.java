package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class ImageButtonFactory {

	public static ImageButton create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (ImageButton) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasSkin = false;
		Skin skin = null;

		boolean hasStyleName = false;
		String styleName = null;

		boolean hasStyle = false;
		ImageButton.ImageButtonStyle style = null;

		boolean hasImageUp = false;
		Drawable imageUp = null;

		boolean hasImageDown = false;
		Drawable imageDown = null;

		boolean hasImageChecked = false;
		Drawable imageChecked = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_SKIN.equals(e.getTagName())) {
					hasSkin = true;
					skin = SkinFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_STYLE_NAME.equals(e.getTagName())) {
					hasStyleName = true;
					styleName = e.getTextContent();
				} else if (XMLFactory.ELEMENT_STYLE.equals(e.getTagName())) {
					hasStyle = true;
					style = ImageButtonStyleFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_IMAGE_UP.equals(e.getTagName())) {
					hasImageUp = true;
					imageUp = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_IMAGE_CHECKED.equals(e.getTagName())) {
					hasImageChecked = true;
					imageChecked = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_IMAGE_DOWN.equals(e.getTagName())) {
					hasImageDown = true;
					imageDown = DrawableFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();
		ImageButton object = null;
		if (hasImageUp && hasImageDown && hasImageChecked) {
			object = new ImageButton(imageUp, imageDown, imageChecked);
			memory.add(XMLFactory.ELEMENT_IMAGE_UP);
			memory.add(XMLFactory.ELEMENT_IMAGE_DOWN);
			memory.add(XMLFactory.ELEMENT_IMAGE_CHECKED);
		} else if (hasImageUp && hasImageDown) {
			object = new ImageButton(imageUp, imageDown);
			memory.add(XMLFactory.ELEMENT_IMAGE_UP);
			memory.add(XMLFactory.ELEMENT_IMAGE_DOWN);
		} else if (hasImageUp) {
			object = new ImageButton(imageUp);
			memory.add(XMLFactory.ELEMENT_IMAGE_UP);
		} else if (hasStyle) {
			object = new ImageButton(style);
			memory.add(XMLFactory.ELEMENT_STYLE);
		} else if (hasSkin && hasStyleName) {
			object = new ImageButton(skin, styleName);
			memory.add(XMLFactory.ELEMENT_STYLE_NAME);
		} else if (hasSkin) {
			object = new ImageButton(skin);
			memory.add(XMLFactory.ELEMENT_SKIN);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else {
					throw new GdxRuntimeException(String.format("constructor %s invalid", e.getTagName()));
				}
			}
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_DISABLED) != null) {
			boolean disabled = Boolean.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_DISABLED).getValue());
			object.setDisabled(disabled);
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_CHECKED) != null) {
			boolean checked = Boolean.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_CHECKED).getValue());
			object.setChecked(checked);
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_PAD) != null) {
			float pad = Float.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_PAD).getValue());
			object.pad(pad);
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
