package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class CheckBoxFactory {

	public static CheckBox create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (CheckBox) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasSkin = false;
		Skin skin = null;

		boolean hasStyleName = false;
		String styleName = null;

		boolean hasStyle = false;
		CheckBox.CheckBoxStyle style = null;

		boolean hasText = false;
		String text = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_SKIN.equals(e.getTagName())) {
					hasSkin = true;
					skin = SkinFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_STYLE_NAME.equals(e.getTagName())) {
					hasStyleName = true;
					styleName = e.getTextContent();
				} else if (XMLFactory.ELEMENT_STYLE.equals(e.getTagName())) {
					hasStyle = true;
					style = CheckBoxStyleFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_TEXT.equals(e.getTagName())) {
					hasText = true;
					text = e.getTextContent();
				}
			}
		}

		Array<String> memory = new Array<String>();

		CheckBox object = null;
		if (hasSkin && hasStyleName && hasText) {
			object = new CheckBox(text, skin, styleName);
			memory.add(XMLFactory.ELEMENT_TEXT);
			memory.add(XMLFactory.ELEMENT_SKIN);
			memory.add(XMLFactory.ELEMENT_STYLE_NAME);
		} else if (hasText && hasSkin) {
			object = new CheckBox(text, skin);
			memory.add(XMLFactory.ELEMENT_TEXT);
			memory.add(XMLFactory.ELEMENT_SKIN);
		} else if (hasText && hasStyle) {
			object = new CheckBox(text, style);
			memory.add(XMLFactory.ELEMENT_TEXT);
			memory.add(XMLFactory.ELEMENT_STYLE);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				}
			}
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_DISABLED) != null) {
			boolean disabled = Boolean.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_DISABLED).getValue());
			object.setDisabled(disabled);
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_CHECKED) != null) {
			boolean checked = Boolean.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_CHECKED).getValue());
			object.setChecked(checked);
		}

		if (element.getAttributeNode(XMLFactory.ATTRIBUTE_PAD) != null) {
			float pad = Float.valueOf(element.getAttributeNode(XMLFactory.ATTRIBUTE_PAD).getValue());
			object.pad(pad);
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
