package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.CpuSpriteBatch;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class BatchFactory {

	public static Batch create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Batch) XMLFactory.findRef(registry, element, ref);
		}

		Batch object = null;

		String type = element.getAttribute(XMLFactory.ATTRIBUTE_TYPE);
		if (XMLFactory.ATTRIBUTE_TYPE_SPRITE.equals(type)) {
			object = createSpriteBatch(registry, element);
		} else if (XMLFactory.ATTRIBUTE_TYPE_CPU_SPRITE.equals(type)) {
			object = createCpuSpriteBatch(registry, element);
		} else if (XMLFactory.ATTRIBUTE_TYPE_POLYGON_SPRITE.equals(type)) {
			object = createPolygonSpriteBatch(registry, element);
		} else {
			throw new GdxRuntimeException(String.format("wrong type %s", type));
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

	private static Batch createSpriteBatch(ArrayMap<String, Object> registry, Element element) {

		// constructor section
		boolean hasSize = false;
		int size = -1;

		boolean hasDefaultShader = false;
		ShaderProgram defaultShader = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_SIZE.equals(e.getTagName())) {
					hasSize = true;
					size = Integer.valueOf(e.getTextContent());
				} else if (XMLFactory.ELEMENT_DEFAULT_SHADER.equals(e.getTagName())) {
					hasDefaultShader = true;
					defaultShader = ShaderProgramFactory.create(registry, e);
				}
			}
		}
		if (!hasSize && !hasDefaultShader) {
			return new SpriteBatch();
		} else if (hasSize && hasDefaultShader) {
			return new SpriteBatch(size, defaultShader);
		} else if (hasSize) {
			return new SpriteBatch(size);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", element.getTagName()));
		}
	}

	private static Batch createPolygonSpriteBatch(ArrayMap<String, Object> registry, Element element) {

		// constructor section

		boolean hasSize = false;
		int size = -1;

		boolean hasDefaultShader = false;
		ShaderProgram defaultShader = null;

		boolean hasMaxVertices = false;
		int maxVertices = -1;

		boolean hasMaxTriangles = false;
		int maxTriangles = -1;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_SIZE.equals(e.getTagName())) {
					hasSize = true;
					size = Integer.valueOf(e.getTextContent());
				} else if (XMLFactory.ELEMENT_DEFAULT_SHADER.equals(e.getTagName())) {
					hasDefaultShader = true;
					defaultShader = ShaderProgramFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_MAXIMUM_VERTICES.equals(e.getTagName())) {
					hasMaxVertices = true;
					maxVertices = Integer.valueOf(e.getTextContent());
				} else if (XMLFactory.ELEMENT_MAXIMUM_TRIANGLES.equals(e.getTagName())) {
					hasMaxTriangles = true;
					maxTriangles = Integer.valueOf(e.getTextContent());
				}
			}
		}

		if (!hasSize && !hasDefaultShader && !hasMaxVertices && !hasMaxTriangles) {
			return new PolygonSpriteBatch();
		} else if (hasDefaultShader && hasMaxVertices && hasMaxTriangles) {
			return new PolygonSpriteBatch(maxVertices, maxTriangles, defaultShader);
		} else if (hasDefaultShader && hasSize) {
			return new PolygonSpriteBatch(size, defaultShader);
		} else if (hasSize) {
			return new PolygonSpriteBatch(size);
		} else {
			throw new GdxRuntimeException(String.format("wrong element %s", element.getTagName()));
		}
	}

	private static Batch createCpuSpriteBatch(ArrayMap<String, Object> registry, Element element) {

		// constructor section

		boolean hasSize = false;
		int size = -1;

		boolean hasDefaultShader = false;
		ShaderProgram defaultShader = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_SIZE.equals(e.getTagName())) {
					hasSize = true;
					size = Integer.valueOf(e.getTextContent());
				} else if (XMLFactory.ELEMENT_DEFAULT_SHADER.equals(e.getTagName())) {
					hasDefaultShader = true;
					defaultShader = ShaderProgramFactory.create(registry, e);
				}
			}
		}
		if (!hasSize && !hasDefaultShader) {
			return new CpuSpriteBatch();
		} else if (hasSize && hasDefaultShader) {
			return new CpuSpriteBatch(size, defaultShader);
		} else if (hasSize) {
			return new CpuSpriteBatch(size);
		} else {
			throw new GdxRuntimeException(String.format("wrong element %s", element.getTagName()));
		}
	}

}
