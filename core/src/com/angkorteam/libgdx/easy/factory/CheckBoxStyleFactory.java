package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class CheckBoxStyleFactory {

	public static CheckBox.CheckBoxStyle create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (CheckBox.CheckBoxStyle) XMLFactory.findRef(registry, element, ref);
		}

		// constructor section

		boolean hasCheckboxOff = false;
		Drawable checkboxOff = null;

		boolean hasCheckboxOn = false;
		Drawable checkboxOn = null;

		boolean hasFont = false;
		BitmapFont font = null;

		boolean hasFontColor = false;
		Color fontColor = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_CHECKBOX_OFF.equals(e.getTagName())) {
					hasCheckboxOff = true;
					checkboxOff = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_CHECKBOX_ON.equals(e.getTagName())) {
					hasCheckboxOn = true;
					checkboxOn = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_FONT.equals(e.getTagName())) {
					hasFont = true;
					font = FontFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_FONT_COLOR.equals(e.getTagName())) {
					hasFontColor = true;
					fontColor = ColorFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();

		CheckBox.CheckBoxStyle object = null;

		if (hasCheckboxOff && hasCheckboxOn && hasFont && hasFontColor) {
			object = new CheckBox.CheckBoxStyle(checkboxOff, checkboxOn, font, fontColor);
			memory.add(XMLFactory.ELEMENT_CHECKBOX_OFF);
			memory.add(XMLFactory.ELEMENT_CHECKBOX_ON);
			memory.add(XMLFactory.ELEMENT_FONT);
			memory.add(XMLFactory.ELEMENT_FONT_COLOR);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		// setter section

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else if (XMLFactory.ELEMENT_DOWN_FONT_COLOR.equals(e.getTagName())) {
					object.downFontColor = ColorFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_OVER_FONT_COLOR.equals(e.getTagName())) {
					object.overFontColor = ColorFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_CHECKED_FONT_COLOR.equals(e.getTagName())) {
					object.checkedFontColor = ColorFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_CHECKED_OVER_FONT_COLOR.equals(e.getTagName())) {
					object.checkedOverFontColor = ColorFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_DISABLED_FONT_COLOR.equals(e.getTagName())) {
					object.disabledFontColor = ColorFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_OVER.equals(e.getTagName())) {
					object.over = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_CHECKED_OVER.equals(e.getTagName())) {
					object.checkedOver = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_DISABLED.equals(e.getTagName())) {
					object.disabled = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_PRESSED_OFFSET_X.equals(e.getTagName())) {
					object.pressedOffsetX = Float.valueOf(e.getTextContent());
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_PRESSED_OFFSET_Y.equals(e.getTagName())) {
					object.pressedOffsetY = Float.valueOf(e.getTextContent());
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_UNPRESSED_OFFSET_X.equals(e.getTagName())) {
					object.unpressedOffsetX = Float.valueOf(e.getTextContent());
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_UNPRESSED_OFFSET_Y.equals(e.getTagName())) {
					object.unpressedOffsetY = Float.valueOf(e.getTextContent());
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_CHECKED_OFFSET_Y.equals(e.getTagName())) {
					object.checkedOffsetY = Float.valueOf(e.getTextContent());
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_CHECKED_OFFSET_X.equals(e.getTagName())) {
					object.checkedOffsetX = Float.valueOf(e.getTextContent());
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_CHECKBOX_OVER.equals(e.getTagName())) {
					object.checkboxOver = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_CHECKBOX_ON_DISABLED.equals(e.getTagName())) {
					object.checkboxOnDisabled = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_CHECKBOX_OFF_DISABLED.equals(e.getTagName())) {
					object.checkboxOffDisabled = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_UP.equals(e.getTagName())) {
					object.up = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_DOWN.equals(e.getTagName())) {
					object.down = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_OVER.equals(e.getTagName())) {
					object.over = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_CHECKED.equals(e.getTagName())) {
					object.checked = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else {
					throw new GdxRuntimeException(String.format("constructor %s invalid", e.getTagName()));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
