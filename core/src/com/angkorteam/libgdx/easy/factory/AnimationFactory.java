package com.angkorteam.libgdx.easy.factory;

import org.mariuszgromada.math.mxparser.Argument;
import org.mariuszgromada.math.mxparser.Expression;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Animation;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.Scaling;

public class AnimationFactory {

	public static Animation create(Array<Argument> argument, ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Animation) XMLFactory.findRef(registry, element, ref);
		}

		// constructor section

		boolean hasSpeed = false;
		float speed = 0;

		boolean hasScaling = false;
		Scaling scaling = null;

		boolean hasAlign = false;
		int align = -1;

		boolean hasItems = false;
		Array<Drawable> items = new Array<Drawable>();

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_SCALING.equals(e.getTagName())) {
					hasScaling = true;
					scaling = Scaling.valueOf(e.getTextContent().trim());
				} else if (XMLFactory.ELEMENT_ALIGN.equals(e.getTagName())) {
					hasAlign = true;
					align = Integer.valueOf(e.getTextContent().trim());
				} else if (XMLFactory.ELEMENT_SPEED.equals(e.getTagName())) {
					hasSpeed = true;
					speed = (float) new Expression(e.getTextContent().trim()).calculate();
				} else if (XMLFactory.ELEMENT_DRAWABLE.equals(e.getTagName())) {
					hasItems = true;
					items.add(DrawableFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_REGION.equals(e.getTagName())) {
					hasItems = true;
					items.add(new TextureRegionDrawable(TextureRegionFactory.create(registry, e)));
				} else if (XMLFactory.ELEMENT_SPRITE.equals(e.getTagName())) {
					hasItems = true;
					items.add(new SpriteDrawable(SpriteFactory.create(registry, e)));
				} else if (XMLFactory.ELEMENT_TEXTURE.equals(e.getTagName())) {
					hasItems = true;
					items.add(new TextureRegionDrawable(new TextureRegion(TextureFactory.create(registry, e))));
				}
			}
		}

		Array<String> memory = new Array<String>();

		Animation object = null;
		if (hasSpeed && hasScaling && hasAlign && hasItems) {
			object = new Animation(speed, items, scaling, align);
			memory.add(XMLFactory.ELEMENT_SPEED);
			memory.add(XMLFactory.ELEMENT_SCALING);
			memory.add(XMLFactory.ELEMENT_ALIGN);
			memory.add(XMLFactory.ELEMENT_DRAWABLE);
			memory.add(XMLFactory.ELEMENT_REGION);
			memory.add(XMLFactory.ELEMENT_SPRITE);
			memory.add(XMLFactory.ELEMENT_TEXTURE);
		} else if (hasSpeed && hasScaling && hasItems) {
			object = new Animation(speed, items, scaling);
			memory.add(XMLFactory.ELEMENT_SPEED);
			memory.add(XMLFactory.ELEMENT_SCALING);
			memory.add(XMLFactory.ELEMENT_DRAWABLE);
			memory.add(XMLFactory.ELEMENT_REGION);
			memory.add(XMLFactory.ELEMENT_SPRITE);
			memory.add(XMLFactory.ELEMENT_TEXTURE);
		} else if (hasSpeed && hasItems) {
			object = new Animation(speed, items);
			memory.add(XMLFactory.ELEMENT_SPEED);
			memory.add(XMLFactory.ELEMENT_DRAWABLE);
			memory.add(XMLFactory.ELEMENT_REGION);
			memory.add(XMLFactory.ELEMENT_SPRITE);
			memory.add(XMLFactory.ELEMENT_TEXTURE);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else if (XMLFactory.ELEMENT_POSITION.equals(e.getTagName())) {
					memory.add(e.getTagName());
					ActorFactory.setPosition(argument, e, object);
				} else if (XMLFactory.ELEMENT_SIZE.equals(e.getTagName())) {
					memory.add(e.getTagName());
					ActorFactory.setSize(e, object);
				} else if (XMLFactory.ELEMENT_COLOR.equals(e.getTagName())) {
					memory.add(e.getTagName());
					ActorFactory.setColor(registry, e, object);
				} else if (XMLFactory.ELEMENT_TIMELINE.equals(e.getTagName())) {
					memory.add(e.getTagName());
					ActorFactory.setAction(registry, e, object);
				} else if (XMLFactory.ELEMENT_SCALE.equals(e.getTagName())) {
					memory.add(e.getTagName());
					ActorFactory.setScale(e, object);
				} else {
					throw new GdxRuntimeException(String.format("tag %s invalid", tagName));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
