package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.angkorteam.libgdx.easy.validation.InterpolationValidator;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleByAction;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class ScaleByActionFactory {

	public static ScaleByAction create(ArrayMap<String, Object> registry, Element element) {
		Attr amountWidth = element.getAttributeNode(XMLFactory.ATTRIBUTE_AMOUNT_WIDTH);
		Attr amountHeight = element.getAttributeNode(XMLFactory.ATTRIBUTE_AMOUNT_HEIGHT);
		Attr duration = element.getAttributeNode(XMLFactory.ATTRIBUTE_DURATION);
		Attr interpolation = element.getAttributeNode(XMLFactory.ATTRIBUTE_INTERPOLATION);
		if (amountWidth != null && amountHeight != null && duration != null && interpolation != null) {
			return Actions.scaleBy(Float.valueOf(amountWidth.getValue()), Float.valueOf(amountHeight.getValue()),
					Float.valueOf(duration.getValue()),
					InterpolationValidator.parse(interpolation.getTextContent().trim()));
		} else if (amountWidth != null && amountHeight != null && duration != null) {
			return Actions.scaleBy(Float.valueOf(amountWidth.getValue()), Float.valueOf(amountHeight.getValue()),
					Float.valueOf(duration.getValue()));
		} else if (amountWidth != null && amountHeight != null) {
			return Actions.scaleBy(Float.valueOf(amountWidth.getValue()), Float.valueOf(amountHeight.getValue()));
		} else {
			throw new GdxRuntimeException(String.format("%s invalid", element.getTagName()));
		}
	}

}
