package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.FlipAction;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class FlipActionFactory {

	public static FlipAction create(ArrayMap<String, Object> registry, Element element) {
		Attr flipX = element.getAttributeNode(XMLFactory.ATTRIBUTE_FLIP_X);
		Attr flipY = element.getAttributeNode(XMLFactory.ATTRIBUTE_FLIP_Y);
		if (flipX != null && flipY != null) {
			FlipAction flipAction = Actions.action(FlipAction.class);
			flipAction.flip(Boolean.valueOf(flipX.getValue()), Boolean.valueOf(flipY.getValue()));
			return flipAction;
		} else {
			throw new GdxRuntimeException(String.format("%s invalid", element.getTagName()));
		}
	}

}
