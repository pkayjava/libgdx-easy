package com.angkorteam.libgdx.easy.factory;

import org.mariuszgromada.math.mxparser.Argument;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.angkorteam.libgdx.easy.validation.AlignmentValidator;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class LabelFactory {

	public static Label create(Array<Argument> argument, ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Label) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasText = false;
		String text = null;

		boolean hasSkin = false;
		Skin skin = null;

		boolean hasStyleName = false;
		String styleName = null;

		boolean hasFontName = false;
		String fontName = null;

		boolean hasColor = false;
		Color color = null;

		boolean hasColorName = false;
		String colorName = null;

		boolean hasStyle = false;
		Label.LabelStyle style = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_TEXT.equals(e.getTagName())) {
					hasText = true;
					text = e.getTextContent().trim();
				} else if (XMLFactory.ELEMENT_SKIN.equals(e.getTagName())) {
					hasSkin = true;
					skin = SkinFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_STYLE_NAME.equals(e.getTagName())) {
					hasStyleName = true;
					styleName = e.getTextContent();
				} else if (XMLFactory.ELEMENT_FONT_NAME.equals(e.getTagName())) {
					hasFontName = true;
					fontName = e.getTextContent();
				} else if (XMLFactory.ELEMENT_COLOR.equals(e.getTagName())) {
					hasColor = true;
					color = ColorFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_COLOR_NAME.equals(e.getTagName())) {
					hasColorName = true;
					colorName = e.getTextContent();
				} else if (XMLFactory.ELEMENT_STYLE.equals(e.getTagName())) {
					hasStyle = true;
					style = LabelStyleFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();
		Label object = null;
		if (hasText && hasSkin && hasFontName && hasColorName) {
			object = new Label(text, skin, fontName, colorName);
			memory.add(XMLFactory.ELEMENT_TEXT);
			memory.add(XMLFactory.ELEMENT_SKIN);
			memory.add(XMLFactory.ELEMENT_FONT_NAME);
			memory.add(XMLFactory.ELEMENT_COLOR_NAME);
		} else if (hasText && hasSkin && hasFontName && hasColor) {
			object = new Label(text, skin, fontName, color);
			memory.add(XMLFactory.ELEMENT_TEXT);
			memory.add(XMLFactory.ELEMENT_SKIN);
			memory.add(XMLFactory.ELEMENT_FONT_NAME);
			memory.add(XMLFactory.ELEMENT_COLOR);
		} else if (hasText && hasSkin && hasStyleName) {
			object = new Label(text, skin, styleName);
			memory.add(XMLFactory.ELEMENT_TEXT);
			memory.add(XMLFactory.ELEMENT_SKIN);
			memory.add(XMLFactory.ELEMENT_STYLE_NAME);
		} else if (hasText && hasSkin) {
			object = new Label(text, skin);
			memory.add(XMLFactory.ELEMENT_TEXT);
			memory.add(XMLFactory.ELEMENT_SKIN);
		} else if (hasText && hasStyle) {
			object = new Label(text, style);
			memory.add(XMLFactory.ELEMENT_TEXT);
			memory.add(XMLFactory.ELEMENT_STYLE);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else if (XMLFactory.ELEMENT_TEXT.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.setText(e.getTextContent().trim());
				} else if (XMLFactory.ELEMENT_COLOR.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.setColor(ColorFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_STYLE.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.setStyle(LabelStyleFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_POSITION.equals(e.getTagName())) {
					memory.add(e.getTagName());
					ActorFactory.setPosition(argument, e, object);
				} else if (XMLFactory.ELEMENT_ALIGNMENT.equals(e.getTagName())) {
					memory.add(e.getTagName());
					Attr labelAlign = e.getAttributeNode(XMLFactory.ATTRIBUTE_LABEL_ALIGN);
					Attr lineAlign = e.getAttributeNode(XMLFactory.ATTRIBUTE_LINE_ALIGN);
					Attr alignment = e.getAttributeNode(XMLFactory.ATTRIBUTE_ALIGNMENT);
					if (lineAlign != null && labelAlign != null) {
						int a = Integer.valueOf(labelAlign.getValue());
						int b = Integer.valueOf(lineAlign.getValue());
						object.setAlignment(AlignmentValidator.parse(a), AlignmentValidator.parse(b));
					} else if (alignment != null) {
						int a = Integer.valueOf(alignment.getValue());
						object.setAlignment(AlignmentValidator.parse(a));
					} else {
						throw new GdxRuntimeException(String.format("%s invalid %s", tagName, e.getTagName()));
					}
				} else if (XMLFactory.ELEMENT_FONT_SCALE.equals(e.getTagName())) {
					memory.add(e.getTagName());
					Attr fontScale = e.getAttributeNode(XMLFactory.ATTRIBUTE_FONT_SCALE);
					Attr fontScaleX = e.getAttributeNode(XMLFactory.ATTRIBUTE_FONT_SCALE_X);
					Attr fontScaleY = e.getAttributeNode(XMLFactory.ATTRIBUTE_FONT_SCALE_Y);
					if (fontScaleX != null && fontScaleY != null) {
						object.setFontScale(Float.valueOf(fontScaleX.getValue()), Float.valueOf(fontScaleY.getValue()));
					} else if (fontScale != null) {
						object.setFontScale(Float.valueOf(fontScale.getValue()));
					} else {
						throw new GdxRuntimeException(String.format("%s invalid %s", tagName, e.getTagName()));
					}
				} else if (XMLFactory.ELEMENT_VISIBLE.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.setVisible(Boolean.valueOf(e.getTextContent().trim()));
				} else if (XMLFactory.ELEMENT_USER_OBJECT.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.setUserObject(e.getTextContent().trim());
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
