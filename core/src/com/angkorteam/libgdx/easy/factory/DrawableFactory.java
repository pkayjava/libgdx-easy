package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class DrawableFactory {

	public static Drawable create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		String type = element.getAttribute(XMLFactory.ATTRIBUTE_TYPE);
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Drawable) XMLFactory.findRef(registry, element, ref);
		}

		Drawable object = null;
		if (XMLFactory.ATTRIBUTE_TYPE_SPRITE.equals(type)) {
			object = createSpriteDrawable(registry, element);
		} else if (XMLFactory.ATTRIBUTE_TYPE_NINE_PATCH.equals(type)) {
			object = createNinePatchDrawable(registry, element);
		} else if (XMLFactory.ATTRIBUTE_TYPE_TILED.equals(type)) {
			object = createTiledDrawable(registry, element);
		} else if (XMLFactory.ATTRIBUTE_TYPE_TEXTURE_REGION.equals(type)) {
			object = createTextureRegionDrawable(registry, element);
		} else {
			throw new GdxRuntimeException(String.format("wrong type %s", type));
		}

		Attr padTop = element.getAttributeNode(XMLFactory.ATTRIBUTE_PAD_TOP);
		if (padTop != null) {
			object.setTopHeight(Float.valueOf(padTop.getValue()));
		}

		Attr padRight = element.getAttributeNode(XMLFactory.ATTRIBUTE_PAD_RIGHT);
		if (padRight != null) {
			object.setRightWidth(Float.valueOf(padRight.getValue()));
		}

		Attr padBottom = element.getAttributeNode(XMLFactory.ATTRIBUTE_PAD_BOTTOM);
		if (padBottom != null) {
			object.setBottomHeight(Float.valueOf(padBottom.getValue()));
		}

		Attr padLeft = element.getAttributeNode(XMLFactory.ATTRIBUTE_PAD_LEFT);
		if (padBottom != null) {
			object.setLeftWidth(Float.valueOf(padLeft.getValue()));
		}

		Attr padding = element.getAttributeNode(XMLFactory.ATTRIBUTE_PADDING);
		if (padding != null) {
			float pad = Float.valueOf(padding.getValue());
			object.setTopHeight(pad);
			object.setRightWidth(pad);
			object.setBottomHeight(pad);
			object.setLeftWidth(pad);
		}

		XMLFactory.register(registry, element, object);

		return object;

	}

	private static Drawable createSpriteDrawable(ArrayMap<String, Object> registry, Element element) {
		String texture = element.getTextContent().trim();
		Sprite sprite = new Sprite(new Texture(texture));
		return new SpriteDrawable(sprite);
	}

	private static Drawable createNinePatchDrawable(ArrayMap<String, Object> registry, Element element) {
		String texture = element.getTextContent().trim();
		int left = Integer.valueOf(element.getAttribute(XMLFactory.ATTRIBUTE_LEFT));
		int right = Integer.valueOf(element.getAttribute(XMLFactory.ATTRIBUTE_RIGHT));
		int top = Integer.valueOf(element.getAttribute(XMLFactory.ATTRIBUTE_TOP));
		int bottom = Integer.valueOf(element.getAttribute(XMLFactory.ATTRIBUTE_BOTTOM));
		NinePatch patch = new NinePatch(new TextureRegion(new Texture(texture)), left, right, top, bottom);
		return new NinePatchDrawable(patch);
	}

	private static TiledDrawable createTiledDrawable(ArrayMap<String, Object> registry, Element element) {
		String texture = element.getTextContent().trim();
		return new TiledDrawable(new TextureRegion(new Texture(texture)));
	}

	private static TextureRegionDrawable createTextureRegionDrawable(ArrayMap<String, Object> registry,
			Element element) {
		String texture = element.getTextContent().trim();
		return new TextureRegionDrawable(new TextureRegion(new Texture(texture)));
	}

}
