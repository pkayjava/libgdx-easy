package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class SliderStyleFactory {

	public static Slider.SliderStyle create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Slider.SliderStyle) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasBackground = false;
		Drawable background = null;

		boolean hasKnob = false;
		Drawable knob = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_BACKGROUND.equals(e.getTagName())) {
					hasBackground = true;
					background = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_KNOB.equals(e.getTagName())) {
					hasKnob = true;
					knob = DrawableFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();
		Slider.SliderStyle object = null;
		if (hasBackground && hasKnob) {
			object = new Slider.SliderStyle(background, knob);
			memory.add(XMLFactory.ELEMENT_BACKGROUND);
			memory.add(XMLFactory.ELEMENT_KNOB);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else if (XMLFactory.ELEMENT_DISABLED_BACKGROUND.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.disabledBackground = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_DISABLED_KNOB.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.disabledKnob = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_KNOB_BEFORE.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.knobBefore = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_KNOB_AFTER.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.knobAfter = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_DISABLED_KNOB_BEFORE.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.disabledKnobBefore = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_DISABLED_KNOB_AFTER.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.disabledKnobAfter = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_KNOB_OVER.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.knobOver = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_KNOB_DOWN.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.knobDown = DrawableFactory.create(registry, e);
				} else {
					throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
