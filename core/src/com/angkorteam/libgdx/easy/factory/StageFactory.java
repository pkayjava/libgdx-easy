package com.angkorteam.libgdx.easy.factory;

import org.mariuszgromada.math.mxparser.Argument;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.viewport.Viewport;

public class StageFactory {

	public static Stage create(Array<Argument> argument, ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Stage) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasViewport = false;
		Viewport viewport = null;

		boolean hasBatch = false;
		Batch batch = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_BATCH.equals(e.getTagName())) {
					hasBatch = true;
					batch = BatchFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_VIEWPORT.equals(e.getTagName())) {
					hasViewport = true;
					viewport = ViewportFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();
		Stage object = null;
		if (hasBatch && hasViewport) {
			object = new Stage(viewport, batch);
			memory.add(XMLFactory.ELEMENT_VIEWPORT);
			memory.add(XMLFactory.ELEMENT_BATCH);
		} else if (!hasBatch && !hasViewport) {
			object = new Stage();
		} else if (hasViewport) {
			object = new Stage(viewport);
			memory.add(XMLFactory.ELEMENT_VIEWPORT);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else if (XMLFactory.ELEMENT_LABEL.equals(e.getTagName())) {
					object.addActor(LabelFactory.create(argument, registry, e));
				} else if (XMLFactory.ELEMENT_TEXT_FIELD.equals(e.getTagName())) {
					object.addActor(TextFieldFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_TABLE.equals(e.getTagName())) {
					object.addActor(TableFactory.create(argument, registry, e));
				} else if (XMLFactory.ELEMENT_FONT.equals(e.getTagName())) {
					object.addActor(TableFactory.create(argument, registry, e));
				} else if (XMLFactory.ELEMENT_SKIN.equals(e.getTagName())) {
					SkinFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_IMAGE.equals(e.getTagName())) {
					object.addActor(ImageFactory.create(argument, registry, e));
				} else if (XMLFactory.ELEMENT_ANIMATION.equals(e.getTagName())) {
					object.addActor(AnimationFactory.create(argument, registry, e));
				} else if (XMLFactory.ELEMENT_CHARACTER.equals(e.getTagName())) {
					object.addActor(CharacterFactory.create(argument, registry, e));
				} else {
					throw new GdxRuntimeException(String.format("unknown tag %s", e.getTagName()));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
