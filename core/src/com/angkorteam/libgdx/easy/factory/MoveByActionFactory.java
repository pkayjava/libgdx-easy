package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.angkorteam.libgdx.easy.validation.InterpolationValidator;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class MoveByActionFactory {

	public static MoveByAction create(ArrayMap<String, Object> registry, Element element) {
		Attr amountX = element.getAttributeNode(XMLFactory.ATTRIBUTE_AMOUNT_X);
		Attr amountY = element.getAttributeNode(XMLFactory.ATTRIBUTE_AMOUNT_Y);
		Attr duration = element.getAttributeNode(XMLFactory.ATTRIBUTE_DURATION);
		Attr interpolation = element.getAttributeNode(XMLFactory.ATTRIBUTE_INTERPOLATION);
		if (amountX != null && amountY != null && duration != null && interpolation != null) {
			return Actions.moveBy(Float.valueOf(amountX.getValue()), Float.valueOf(amountY.getValue()),
					Float.valueOf(duration.getValue()),
					InterpolationValidator.parse(interpolation.getTextContent().trim()));
		} else if (amountX != null && amountY != null && duration != null) {
			return Actions.moveBy(Float.valueOf(amountX.getValue()), Float.valueOf(amountY.getValue()),
					Float.valueOf(duration.getValue()));
		} else if (amountX != null && amountY != null) {
			return Actions.moveBy(Float.valueOf(amountX.getValue()), Float.valueOf(amountY.getValue()));
		} else {
			throw new GdxRuntimeException(String.format("%s invalid", element.getTagName()));
		}
	}

}
