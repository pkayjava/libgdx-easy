package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class ListStyleFactory {

	public static List.ListStyle create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (List.ListStyle) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasFont = false;
		BitmapFont font = null;

		boolean hasFontColorSelected = false;
		Color fontColorSelected = null;

		boolean hasFontColorUnselected = false;
		Color fontColorUnselected = null;

		boolean hasSelection = false;
		Drawable selection = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_FONT_COLOR_SELECTED.equals(e.getTagName())) {
					hasFontColorSelected = true;
					fontColorSelected = ColorFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_FONT.equals(e.getTagName())) {
					hasFont = true;
					font = FontFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_FONT_COLOR_UNSELECTED.equals(e.getTagName())) {
					hasFontColorUnselected = true;
					fontColorUnselected = ColorFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_SELECTION.equals(e.getTagName())) {
					hasSelection = true;
					selection = DrawableFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();
		List.ListStyle object = null;
		if (hasFont && hasFontColorSelected && hasFontColorUnselected && hasSelection) {
			object = new List.ListStyle(font, fontColorSelected, fontColorUnselected, selection);
			memory.add(XMLFactory.ELEMENT_FONT);
			memory.add(XMLFactory.ELEMENT_FONT_COLOR_SELECTED);
			memory.add(XMLFactory.ELEMENT_FONT_COLOR_UNSELECTED);
			memory.add(XMLFactory.ELEMENT_SELECTION);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else if (XMLFactory.ELEMENT_BACKGROUND.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.background = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_DOWN.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.down = DrawableFactory.create(registry, e);
				} else {
					throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
