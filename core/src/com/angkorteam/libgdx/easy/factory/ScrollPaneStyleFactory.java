package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class ScrollPaneStyleFactory {

	public static ScrollPane.ScrollPaneStyle create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (ScrollPane.ScrollPaneStyle) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasBackground = false;
		Drawable background = null;

		boolean hasHorizontalScroll = false;
		Drawable horizontalScroll = null;

		boolean hasHorizontalScrollKnob = false;
		Drawable horizontalScrollKnob = null;

		boolean hasVerticalScroll = false;
		Drawable verticalScroll = null;

		boolean hasVerticalScrollKnob = false;
		Drawable verticalScrollKnob = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_BACKGROUND.equals(e.getTagName())) {
					hasBackground = true;
					background = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_HORIZONTAL_SCROLL.equals(e.getTagName())) {
					hasHorizontalScroll = true;
					horizontalScroll = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_HORIZONTAL_SCROLL_KNOB.equals(e.getTagName())) {
					hasHorizontalScrollKnob = true;
					horizontalScrollKnob = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_VERTICAL_SCROLL.equals(e.getTagName())) {
					hasVerticalScroll = true;
					verticalScroll = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_VERTICAL_SCROLL_KNOB.equals(e.getTagName())) {
					hasVerticalScrollKnob = true;
					verticalScrollKnob = DrawableFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();
		ScrollPane.ScrollPaneStyle object = null;
		if (hasBackground && hasHorizontalScroll && hasHorizontalScrollKnob && hasVerticalScroll
				&& hasVerticalScrollKnob) {
			object = new ScrollPane.ScrollPaneStyle(background, horizontalScroll, horizontalScrollKnob, verticalScroll,
					verticalScrollKnob);
			memory.add(XMLFactory.ELEMENT_BACKGROUND);
			memory.add(XMLFactory.ELEMENT_HORIZONTAL_SCROLL);
			memory.add(XMLFactory.ELEMENT_HORIZONTAL_SCROLL_KNOB);
			memory.add(XMLFactory.ELEMENT_VERTICAL_SCROLL);
			memory.add(XMLFactory.ELEMENT_VERTICAL_SCROLL_KNOB);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else if (XMLFactory.ELEMENT_CORNER.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.corner = DrawableFactory.create(registry, e);
				} else {
					throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
