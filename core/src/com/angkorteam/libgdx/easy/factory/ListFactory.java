package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class ListFactory {

	public static List<String> create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (List<String>) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasSkin = false;
		Skin skin = null;

		boolean hasStyleName = false;
		String styleName = null;

		boolean hasStyle = false;
		List.ListStyle style = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_SKIN.equals(e.getTagName())) {
					hasSkin = true;
					skin = SkinFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_STYLE_NAME.equals(e.getTagName())) {
					hasStyleName = true;
					styleName = e.getTextContent();
				} else if (XMLFactory.ELEMENT_STYLE.equals(e.getTagName())) {
					hasStyle = true;
					style = ListStyleFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();
		List<String> object = null;
		if (hasSkin && hasStyleName) {
			object = new List<>(skin, styleName);
			memory.add(XMLFactory.ELEMENT_SKIN);
			memory.add(XMLFactory.ELEMENT_STYLE_NAME);
		} else if (hasSkin) {
			object = new List<>(skin);
			memory.add(XMLFactory.ELEMENT_SKIN);
		} else if (hasStyle) {
			object = new List<>(style);
			memory.add(XMLFactory.ELEMENT_STYLE);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else if (XMLFactory.ELEMENT_ITEM.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.getItems().add(e.getTextContent().trim());
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
