package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.angkorteam.libgdx.easy.validation.InterpolationValidator;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class ScaleToActionFactory {

	public static ScaleToAction create(ArrayMap<String, Object> registry, Element element) {
		Attr width = element.getAttributeNode(XMLFactory.ATTRIBUTE_WIDTH);
		Attr height = element.getAttributeNode(XMLFactory.ATTRIBUTE_HEIGHT);
		Attr duration = element.getAttributeNode(XMLFactory.ATTRIBUTE_DURATION);
		Attr interpolation = element.getAttributeNode(XMLFactory.ATTRIBUTE_INTERPOLATION);
		if (width != null && height != null && duration != null && interpolation != null) {
			return Actions.scaleTo(Float.valueOf(width.getValue()), Float.valueOf(height.getValue()),
					Float.valueOf(duration.getValue()),
					InterpolationValidator.parse(interpolation.getTextContent().trim()));
		} else if (width != null && height != null && duration != null) {
			return Actions.scaleTo(Float.valueOf(width.getValue()), Float.valueOf(height.getValue()),
					Float.valueOf(duration.getValue()));
		} else if (width != null && height != null) {
			return Actions.scaleTo(Float.valueOf(width.getValue()), Float.valueOf(height.getValue()));
		} else {
			throw new GdxRuntimeException(String.format("%s invalid", element.getTagName()));
		}
	}

}
