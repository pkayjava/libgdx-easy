package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class ScrollPaneFactory {

	public static ScrollPane create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (ScrollPane) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasWidget = false;
		Actor widget = null;

		boolean hasSkin = false;
		Skin skin = null;

		boolean hasStyleName = false;
		String styleName = null;

		boolean hasStyle = false;
		ScrollPane.ScrollPaneStyle style = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_WIDGET.equals(e.getTagName())) {
					hasWidget = true;
					widget = ActorFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_SKIN.equals(e.getTagName())) {
					hasSkin = true;
					skin = SkinFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_STYLE_NAME.equals(e.getTagName())) {
					hasStyleName = true;
					styleName = e.getTextContent();
				} else if (XMLFactory.ELEMENT_STYLE.equals(e.getTagName())) {
					hasStyle = true;
					style = ScrollPaneStyleFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();
		ScrollPane object = null;
		if (hasWidget && hasSkin && hasStyleName) {
			object = new ScrollPane(widget, skin, styleName);
			memory.add(XMLFactory.ELEMENT_WIDGET);
			memory.add(XMLFactory.ELEMENT_SKIN);
			memory.add(XMLFactory.ELEMENT_STYLE_NAME);
		} else if (hasWidget && hasSkin) {
			object = new ScrollPane(widget, skin);
			memory.add(XMLFactory.ELEMENT_WIDGET);
			memory.add(XMLFactory.ELEMENT_SKIN);
		} else if (hasWidget && hasStyle) {
			object = new ScrollPane(widget, style);
			memory.add(XMLFactory.ELEMENT_WIDGET);
			memory.add(XMLFactory.ELEMENT_STYLE);
		} else if (hasWidget) {
			object = new ScrollPane(widget);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
