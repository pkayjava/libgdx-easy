package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class TimelineActionFactory {

	public static Action create(ArrayMap<String, Object> registry, Element element) {

		Attr type = element.getAttributeNode(XMLFactory.ATTRIBUTE_TYPE);
		if (type == null || "".equals(type.getValue())
				|| XMLFactory.ATTRIBUTE_TYPE_PARALLEL.equalsIgnoreCase(type.getValue())) {
			ParallelAction object = Actions.parallel();
			NodeList nodes = element.getChildNodes();
			for (int i = 0; i < nodes.getLength(); i++) {
				if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
					Element e = (Element) nodes.item(i);
					object.addAction(createAction(registry, e));
				}
			}
			return object;
		} else if (XMLFactory.ATTRIBUTE_TYPE_SEQUENCE.equalsIgnoreCase(type.getValue())) {
			SequenceAction object = Actions.sequence();
			NodeList nodes = element.getChildNodes();
			for (int i = 0; i < nodes.getLength(); i++) {
				if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
					Element e = (Element) nodes.item(i);
					object.addAction(createAction(registry, e));
				}
			}
			return object;
		} else if (XMLFactory.ATTRIBUTE_TYPE_REPEAT.equalsIgnoreCase(type.getValue())) {
			Attr repeatCount = element.getAttributeNode(XMLFactory.ATTRIBUTE_REPEAT_COUNT);
			if (repeatCount == null) {
				Attr repeatType = element.getAttributeNode(XMLFactory.ATTRIBUTE_REPEAT_TYPE);
				ParallelAction object = null;
				if (XMLFactory.ATTRIBUTE_REPEAT_TYPE_SEQUENCE.equalsIgnoreCase(repeatType.getValue())) {
					object = Actions.sequence();
				} else if (XMLFactory.ATTRIBUTE_REPEAT_TYPE_SEQUENCE.equalsIgnoreCase(repeatType.getValue())) {
					object = Actions.parallel();
				} else {
					throw new GdxRuntimeException(String.format("constructor %s invalid", element.getTagName()));
				}
				NodeList nodes = element.getChildNodes();
				for (int i = 0; i < nodes.getLength(); i++) {
					if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
						Element e = (Element) nodes.item(i);
						object.addAction(createAction(registry, e));
					}
				}
				return Actions.forever(object);
			} else {
				Attr repeatType = element.getAttributeNode(XMLFactory.ATTRIBUTE_REPEAT_TYPE);
				ParallelAction object = null;
				if (XMLFactory.ATTRIBUTE_REPEAT_TYPE_SEQUENCE.equalsIgnoreCase(repeatType.getValue())) {
					object = Actions.sequence();
				} else if (XMLFactory.ATTRIBUTE_REPEAT_TYPE_PARALLEL.equalsIgnoreCase(repeatType.getValue())) {
					object = Actions.parallel();
				} else {
					throw new GdxRuntimeException(String.format("constructor %s invalid", element.getTagName()));
				}
				NodeList nodes = element.getChildNodes();
				for (int i = 0; i < nodes.getLength(); i++) {
					if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
						Element e = (Element) nodes.item(i);
						object.addAction(createAction(registry, e));
					}
				}
				return Actions.repeat(Integer.valueOf(repeatCount.getValue()), object);
			}
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", element.getTagName()));
		}
	}

	protected static Action createAction(ArrayMap<String, Object> registry, Element e) {
		if (XMLFactory.ELEMENT_MOVE_BY.equals(e.getTagName())) {
			return MoveByActionFactory.create(registry, e);
		} else if (XMLFactory.ELEMENT_MOVE_TO.equals(e.getTagName())) {
			return MoveToActionFactory.create(registry, e);
		} else if (XMLFactory.ELEMENT_MOVE_TO_ALIGNED.equals(e.getTagName())) {
			return MoveToAlignedFactory.create(registry, e);
		} else if (XMLFactory.ELEMENT_SIZE_TO.equals(e.getTagName())) {
			return SizeToActionFactory.create(registry, e);
		} else if (XMLFactory.ELEMENT_SIZE_BY.equals(e.getTagName())) {
			return SizeByActionFactory.create(registry, e);
		} else if (XMLFactory.ELEMENT_SCALE_TO.equals(e.getTagName())) {
			return ScaleToActionFactory.create(registry, e);
		} else if (XMLFactory.ELEMENT_SCALE_BY.equals(e.getTagName())) {
			return ScaleByActionFactory.create(registry, e);
		} else if (XMLFactory.ELEMENT_ROTATE_TO.equals(e.getTagName())) {
			return RotateToActionFactory.create(registry, e);
		} else if (XMLFactory.ELEMENT_ROTATE_BY.equals(e.getTagName())) {
			return RotateByActionFactory.create(registry, e);
		} else if (XMLFactory.ELEMENT_COLOR.equals(e.getTagName())) {
			return ColorActionFactory.create(registry, e);
		} else if (XMLFactory.ELEMENT_ALPHA.equals(e.getTagName())) {
			return AlphaActionFactory.create(registry, e);
		} else if (XMLFactory.ELEMENT_FADE_OUT.equals(e.getTagName())) {
			return FadeOutActionFactory.create(registry, e);
		} else if (XMLFactory.ELEMENT_FADE_IN.equals(e.getTagName())) {
			return FadeInActionFactory.create(registry, e);
		} else if (XMLFactory.ELEMENT_ACTION.equals(e.getTagName())) {
			return TimelineActionFactory.create(registry, e);
		} else if (XMLFactory.ELEMENT_FLIP.equals(e.getTagName())) {
			return FlipActionFactory.create(registry, e);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", e.getTagName()));
		}
	}

}
