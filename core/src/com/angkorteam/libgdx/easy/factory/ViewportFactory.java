package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class ViewportFactory {

	public static Viewport create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Viewport) XMLFactory.findRef(registry, element, ref);
		}

		Viewport object = null;

		String type = element.getAttribute(XMLFactory.ATTRIBUTE_TYPE);
		if (XMLFactory.ATTRIBUTE_TYPE_SCREEN.equals(type)) {
			object = createScreenViewport(registry, element);
		} else if (XMLFactory.ATTRIBUTE_TYPE_FILL.equals(type)) {
			object = createFillViewport(registry, element);
		} else if (XMLFactory.ATTRIBUTE_TYPE_FIT.equals(type)) {
			object = createFitViewport(registry, element);
		} else if (XMLFactory.ATTRIBUTE_TYPE_STRETCH.equals(type)) {
			object = createStretchViewport(registry, element);
		} else if (XMLFactory.ATTRIBUTE_TYPE_SCALING.equals(type)) {
			object = createScalingViewport(registry, element);
		} else if (XMLFactory.ATTRIBUTE_TYPE_EXTEND.equals(type)) {
			object = createExtendViewport(registry, element);
		} else {
			throw new GdxRuntimeException(String.format("wrong type %s", type));
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

	private static Viewport createScreenViewport(ArrayMap<String, Object> registry, Element element) {
		boolean hasCamera = false;
		Camera camera = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_CAMERA.equals(e.getTagName())) {
					hasCamera = true;
					camera = CameraFactory.create(registry, e);
				}
			}
		}
		if (hasCamera) {
			return new ScreenViewport(camera);
		} else {
			return new ScreenViewport();
		}
	}

	private static Viewport createFillViewport(ArrayMap<String, Object> registry, Element element) {
		boolean hasCamera = false;
		Camera camera = null;

		boolean hasWorldWidth = false;
		float worldWidth = -1;

		boolean hasWorldHeight = false;
		float worldHeight = -1;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_CAMERA.equals(e.getTagName())) {
					hasCamera = true;
					camera = CameraFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_WORLD_WIDTH.equals(e.getTagName())) {
					worldWidth = Float.valueOf(e.getTextContent());
					hasWorldWidth = true;
				} else if (XMLFactory.ELEMENT_WORLD_HEIGHT.equals(e.getTagName())) {
					worldHeight = Float.valueOf(e.getTextContent());
					hasWorldHeight = true;
				}
			}
		}

		if (hasCamera && hasWorldHeight && hasWorldWidth) {
			return new FillViewport(worldWidth, worldHeight, camera);
		} else if (hasWorldHeight && hasWorldWidth) {
			return new FillViewport(worldWidth, worldHeight);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", element.getTagName()));
		}
	}

	private static Viewport createFitViewport(ArrayMap<String, Object> registry, Element element) {
		boolean hasCamera = false;
		Camera camera = null;

		boolean hasWorldWidth = false;
		float worldWidth = -1;

		boolean hasWorldHeight = false;
		float worldHeight = -1;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_CAMERA.equals(e.getTagName())) {
					hasCamera = true;
					camera = CameraFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_WORLD_WIDTH.equals(e.getTagName())) {
					worldWidth = Float.valueOf(e.getTextContent());
					hasWorldWidth = true;
				} else if (XMLFactory.ELEMENT_WORLD_HEIGHT.equals(e.getTagName())) {
					worldHeight = Float.valueOf(e.getTextContent());
					hasWorldHeight = true;
				}
			}
		}

		if (hasCamera && hasWorldHeight && hasWorldWidth) {
			return new FitViewport(worldWidth, worldHeight, camera);
		} else if (hasWorldHeight && hasWorldWidth) {
			return new FitViewport(worldWidth, worldHeight);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", element.getTagName()));
		}
	}

	private static Viewport createStretchViewport(ArrayMap<String, Object> registry, Element element) {
		boolean hasCamera = false;
		Camera camera = null;

		boolean hasWorldWidth = false;
		float worldWidth = -1;

		boolean hasWorldHeight = false;
		float worldHeight = -1;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_CAMERA.equals(e.getTagName())) {
					hasCamera = true;
					camera = CameraFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_WORLD_WIDTH.equals(e.getTagName())) {
					worldWidth = Float.valueOf(e.getTextContent());
					hasWorldWidth = true;
				} else if (XMLFactory.ELEMENT_WORLD_HEIGHT.equals(e.getTagName())) {
					worldHeight = Float.valueOf(e.getTextContent());
					hasWorldHeight = true;
				}
			}
		}

		if (hasCamera && hasWorldHeight && hasWorldWidth) {
			return new StretchViewport(worldWidth, worldHeight, camera);
		} else if (hasWorldHeight && hasWorldWidth) {
			return new StretchViewport(worldWidth, worldHeight);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", element.getTagName()));
		}
	}

	private static Viewport createScalingViewport(ArrayMap<String, Object> registry, Element element) {
		boolean hasCamera = false;
		Camera camera = null;

		boolean hasWorldWidth = false;
		float worldWidth = -1;

		boolean hasWorldHeight = false;
		float worldHeight = -1;

		boolean hasScaling = false;
		Scaling scaling = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_CAMERA.equals(e.getTagName())) {
					hasCamera = true;
					camera = CameraFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_WORLD_WIDTH.equals(e.getTagName())) {
					worldWidth = Float.valueOf(e.getTextContent());
					hasWorldWidth = true;
				} else if (XMLFactory.ELEMENT_WORLD_HEIGHT.equals(e.getTagName())) {
					worldHeight = Float.valueOf(e.getTextContent());
					hasWorldHeight = true;
				} else if (XMLFactory.ELEMENT_SCALING.equals(e.getTagName())) {
					scaling = Scaling.valueOf(e.getTextContent());
					hasScaling = true;
				}
			}
		}

		if (hasScaling && hasCamera && hasWorldHeight && hasWorldWidth) {
			return new ScalingViewport(scaling, worldWidth, worldHeight, camera);
		} else if (hasScaling && hasWorldHeight && hasWorldWidth) {
			return new ScalingViewport(scaling, worldWidth, worldHeight);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", element.getTagName()));
		}
	}

	private static Viewport createExtendViewport(ArrayMap<String, Object> registry, Element element) {
		boolean hasCamera = false;
		Camera camera = null;

		boolean hasMinWorldWidth = false;
		float minWorldWidth = -1;

		boolean hasMinWorldHeight = false;
		float minWorldHeight = -1;

		boolean hasMaxWorldWidth = false;
		float maxWorldWidth = -1;

		boolean hasMaxWorldHeight = false;
		float maxWorldHeight = -1;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_CAMERA.equals(e.getTagName())) {
					hasCamera = true;
					camera = CameraFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_MAXIMUM_WORLD_HEIGHT.equals(e.getTagName())) {
					maxWorldHeight = Float.valueOf(e.getTextContent());
					hasMaxWorldHeight = true;
				} else if (XMLFactory.ELEMENT_MAXIMUM_WORLD_WIDTH.equals(e.getTagName())) {
					maxWorldWidth = Float.valueOf(e.getTextContent());
					hasMaxWorldWidth = true;
				} else if (XMLFactory.ELEMENT_MINIMUM_WORLD_HEIGHT.equals(e.getTagName())) {
					minWorldHeight = Float.valueOf(e.getTextContent());
					hasMinWorldHeight = true;
				} else if (XMLFactory.ELEMENT_MINIMUM_WORLD_WIDTH.equals(e.getTagName())) {
					minWorldWidth = Float.valueOf(e.getTextContent());
					hasMinWorldWidth = true;
				}
			}
		}

		if (hasMinWorldWidth && hasMinWorldHeight && hasMaxWorldWidth && hasMaxWorldHeight && hasCamera) {
			return new ExtendViewport(minWorldWidth, minWorldHeight, maxWorldWidth, maxWorldHeight, camera);
		} else if (hasMinWorldWidth && hasMinWorldHeight && hasMaxWorldWidth && hasMaxWorldHeight) {
			return new ExtendViewport(minWorldWidth, minWorldHeight, maxWorldWidth, maxWorldHeight);
		} else if (hasMinWorldWidth && hasMinWorldHeight && hasCamera) {
			return new ExtendViewport(minWorldWidth, minWorldHeight, camera);
		} else if (hasMinWorldWidth && hasMinWorldHeight) {
			return new ExtendViewport(minWorldWidth, minWorldHeight);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", element.getTagName()));
		}
	}

}
