package com.angkorteam.libgdx.easy.factory;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class XMLFactory {

	private static final String TAG = XMLFactory.class.getSimpleName();

	// Attributes

	public static final String ATTRIBUTE_ID = "id";

	public static final String ATTRIBUTE_REF = "ref";

	public static final String ATTRIBUTE_DEBUG = "debug";

	public static final String ATTRIBUTE_FILL_PARENT = "fillParent";

	public static final String ATTRIBUTE_CENTER = "center";

	public static final String ATTRIBUTE_PAD_TOP = "padTop";

	public static final String ATTRIBUTE_PAD_RIGHT = "padRight";

	public static final String ATTRIBUTE_PAD_BOTTOM = "padBottom";

	public static final String ATTRIBUTE_PAD_LEFT = "padLeft";

	public static final String ATTRIBUTE_PADDING = "padding";

	public static final String ATTRIBUTE_LEFT = "left";

	public static final String ATTRIBUTE_RIGHT = "right";

	public static final String ATTRIBUTE_TOP = "top";

	public static final String ATTRIBUTE_BOTTOM = "bottom";

	public static final String ATTRIBUTE_NAME = "name";

	public static final String ATTRIBUTE_VALUE = "value";

	public static final String ATTRIBUTE_NAME_DEFAULT = "default";

	public static final String ATTRIBUTE_NAME_CLEAR = "CLEAR";

	public static final String ATTRIBUTE_NAME_BLACK = "BLACK";

	public static final String ATTRIBUTE_NAME_MAROON = "MAROON";

	public static final String ATTRIBUTE_NAME_VIOLET = "VIOLET";

	public static final String ATTRIBUTE_NAME_PURPLE = "PURPLE";

	public static final String ATTRIBUTE_NAME_MAGENTA = "MAGENTA";

	public static final String ATTRIBUTE_NAME_PINK = "PINK";

	public static final String ATTRIBUTE_NAME_SALMON = "SALMON";

	public static final String ATTRIBUTE_NAME_CORAL = "CORAL";

	public static final String ATTRIBUTE_NAME_SCARLET = "SCARLET";

	public static final String ATTRIBUTE_NAME_RED = "RED";

	public static final String ATTRIBUTE_NAME_FIREBRICK = "FIREBRICK";

	public static final String ATTRIBUTE_NAME_TAN = "TAN";

	public static final String ATTRIBUTE_NAME_BROWN = "BROWN";

	public static final String ATTRIBUTE_NAME_ORANGE = "ORANGE";

	public static final String ATTRIBUTE_NAME_GOLDENROD = "GOLDENROD";

	public static final String ATTRIBUTE_NAME_GOLD = "GOLD";

	public static final String ATTRIBUTE_NAME_YELLOW = "YELLOW";

	public static final String ATTRIBUTE_NAME_OLIVE = "OLIVE";

	public static final String ATTRIBUTE_NAME_FOREST = "FOREST";

	public static final String ATTRIBUTE_NAME_LIME = "LIME";

	public static final String ATTRIBUTE_NAME_CHARTREUSE = "CHARTREUSE";

	public static final String ATTRIBUTE_NAME_GREEN = "GREEN";

	public static final String ATTRIBUTE_NAME_TEAL = "TEAL";

	public static final String ATTRIBUTE_NAME_CYAN = "CYAN";

	public static final String ATTRIBUTE_NAME_SKY = "SKY";

	public static final String ATTRIBUTE_NAME_SLATE = "SLATE";

	public static final String ATTRIBUTE_NAME_ROYAL = "ROYAL";

	public static final String ATTRIBUTE_NAME_NAVY = "NAVY";

	public static final String ATTRIBUTE_NAME_BLUE = "BLUE";

	public static final String ATTRIBUTE_NAME_DARK_GRAY = "DARK_GRAY";

	public static final String ATTRIBUTE_NAME_GRAY = "GRAY";

	public static final String ATTRIBUTE_NAME_LIGHT_GRAY = "LIGHT_GRAY";

	public static final String ATTRIBUTE_NAME_WHITE = "WHITE";

	public static final String ATTRIBUTE_TYPE = "type";

	public static final String ATTRIBUTE_TYPE_SCREEN = "Screen";

	public static final String ATTRIBUTE_TYPE_FILL = "Fill";

	public static final String ATTRIBUTE_TYPE_FIT = "Fit";

	public static final String ATTRIBUTE_TYPE_REGION = "Region";

	public static final String ATTRIBUTE_TYPE_STRETCH = "Stretch";

	public static final String ATTRIBUTE_TYPE_SCALING = "Scaling";

	public static final String ATTRIBUTE_TYPE_EXTEND = "Extend";

	public static final String ATTRIBUTE_TYPE_SPRITE = "Sprite";

	public static final String ATTRIBUTE_TYPE_PARALLEL = "Parallel";

	public static final String ATTRIBUTE_TYPE_SEQUENCE = "Sequence";

	public static final String ATTRIBUTE_TYPE_REPEAT = "Repeat";

	public static final String ATTRIBUTE_REPEAT_COUNT = "repeatCount";

	public static final String ATTRIBUTE_REPEAT_TYPE = "repeatType";

	public static final String ATTRIBUTE_REPEAT_TYPE_SEQUENCE = "Sequence";

	public static final String ATTRIBUTE_REPEAT_TYPE_PARALLEL = "Parallel";

	public static final String ATTRIBUTE_TYPE_CPU_SPRITE = "CpuSprite";

	public static final String ATTRIBUTE_TYPE_POLYGON_SPRITE = "PolygonSprite";

	public static final String ATTRIBUTE_TYPE_NINE_PATCH = "NinePatch";

	public static final String ATTRIBUTE_TYPE_TILED = "Tiled";

	public static final String ATTRIBUTE_TYPE_TEXTURE_REGION = "TextureRegion";

	public static final String ATTRIBUTE_TYPE_PERSPECTIVE = "Perspective";

	public static final String ATTRIBUTE_TYPE_ORTHOGRAPHIC = "Orthographic";

	public static final String ATTRIBUTE_ALIGNMENT = "alignment";

	public static final String ATTRIBUTE_FONT_SCALE = "fontScale";

	public static final String ATTRIBUTE_FONT_SCALE_Y = "fontScaleY";

	public static final String ATTRIBUTE_FONT_SCALE_X = "fontScaleX";

	public static final String ATTRIBUTE_X = "x";

	public static final String ATTRIBUTE_Y = "y";

	public static final String ATTRIBUTE_HEIGHT = "height";

	public static final String ATTRIBUTE_WIDTH = "width";

	public static final String ATTRIBUTE_DURATION = "duration";

	public static final String ATTRIBUTE_AMOUNT_HEIGHT = "amountHeight";

	public static final String ATTRIBUTE_AMOUNT_WIDTH = "amountWidth";

	public static final String ATTRIBUTE_ROTATION_AMOUNT = "rotationAmount";

	public static final String ATTRIBUTE_ROTATION = "rotation";

	public static final String ATTRIBUTE_INTERPOLATION = "interpolation";

	public static final String ATTRIBUTE_ALPHA = "alpha";

	public static final String ATTRIBUTE_DISABLED = "disabled";

	public static final String ATTRIBUTE_PASSWORD_MODE = "passwordMode";

	public static final String ATTRIBUTE_PASSWORD_CHARACTER = "passwordCharacter";

	public static final String ATTRIBUTE_CHECKED = "checked";

	public static final String ATTRIBUTE_PAD = "pad";

	public static final String ATTRIBUTE_LABEL_ALIGN = "labelAlign";

	public static final String ATTRIBUTE_LINE_ALIGN = "lineAlign";

	public static final String ATTRIBUTE_AMOUNT_X = "amountX";

	public static final String ATTRIBUTE_AMOUNT_Y = "amountY";

	public static final String ATTRIBUTE_MARGIN = "margin";

	public static final String ATTRIBUTE_MARGIN_BOTTOM = "marginBottom";

	public static final String ATTRIBUTE_MARGIN_LEFT = "marginLeft";

	public static final String ATTRIBUTE_MARGIN_RIGHT = "marginRight";

	public static final String ATTRIBUTE_MARGIN_TOP = "marginTop";

	public static final String ATTRIBUTE_EXPAND_X = "expandX";

	public static final String ATTRIBUTE_EXPAND_Y = "expandY";

	public static final String ATTRIBUTE_FILL_X = "fillX";

	public static final String ATTRIBUTE_FILL_Y = "fillY";

	public static final String ATTRIBUTE_FLIP_X = "flipX";

	public static final String ATTRIBUTE_FLIP_Y = "flipY";

	// Elements

	public static final String ELEMENT_ACTOR = "Actor";

	public static final String ELEMENT_LABEL = "Label";

	public static final String ELEMENT_FLIP = "Flip";

	public static final String ELEMENT_TEXT_FIELD = "TextField";

	public static final String ELEMENT_TABLE = "Table";

	public static final String ELEMENT_BUTTON = "Button";

	public static final String ELEMENT_TEXT_BUTTON = "TextButton";

	public static final String ELEMENT_IMAGE_BUTTON = "ImageButton";

	public static final String ELEMENT_IMAGE_TEXT_BUTTON = "ImageTextButton";

	public static final String ELEMENT_CHECK_BOX = "CheckBox";

	public static final String ELEMENT_IMAGE = "Image";

	public static final String ELEMENT_ANIMATION = "Animation";

	public static final String ELEMENT_CHARACTER = "Character";

	public static final String ELEMENT_LIST = "List";

	public static final String ELEMENT_SELECT_BOX = "SelectBox";

	public static final String ELEMENT_SCROLL_PANE = "ScrollPane";

	public static final String ELEMENT_SIZE = "Size";

	public static final String ELEMENT_MINIMUM = "Minimum";

	public static final String ELEMENT_MAXIMUM = "Maximum";

	public static final String ELEMENT_STEP_SIZE = "StepSize";

	public static final String ELEMENT_VERTICAL = "Vertical";

	public static final String ELEMENT_FONT_COLOR_SELECTED = "FontColorSelected";

	public static final String ELEMENT_FONT_COLOR_UNSELECTED = "FontColorUnselected";

	public static final String ELEMENT_SELECTION = "Selection";

	public static final String ELEMENT_DEFAULT_SHADER = "DefaultShader";

	public static final String ELEMENT_MAXIMUM_VERTICES = "MaximumVertices";

	public static final String ELEMENT_MAXIMUM_TRIANGLES = "MaximumTriangles";

	public static final String ELEMENT_SKIN = "Skin";

	public static final String ELEMENT_DRAWABLE_NAME = "DrawableName";

	public static final String ELEMENT_DRAWABLE = "Drawable";

	public static final String ELEMENT_ROW = "Row";

	public static final String ELEMENT_WIDGET = "Widget";

	public static final String ELEMENT_WRAP_WIDTH = "WrapWidth";

	public static final String ELEMENT_HORIZONTAL_SCROLL = "HorizontalScroll";

	public static final String ELEMENT_HORIZONTAL_SCROLL_KNOB = "HorizontalScrollKnob";

	public static final String ELEMENT_VERTICAL_SCROLL = "VerticalScroll";

	public static final String ELEMENT_VERTICAL_SCROLL_KNOB = "VerticalScrollKnob";

	public static final String ELEMENT_CORNER = "Corner";

	public static final String ELEMENT_TITLE = "Title";

	public static final String ELEMENT_STYLE_NAME = "StyleName";

	public static final String ELEMENT_FONT_NAME = "FontName";

	public static final String ELEMENT_STYLE = "Style";

	public static final String ELEMENT_ITEM = "Item";

	public static final String ELEMENT_POSITION = "Position";

	public static final String ELEMENT_ALIGNMENT = "Alignment";

	public static final String ELEMENT_IMAGE_UP = "ImageUp";

	public static final String ELEMENT_IMAGE_CHECKED = "ImageChecked";

	public static final String ELEMENT_IMAGE_DOWN = "ImageDown";

	public static final String ELEMENT_IMAGE_OVER = "ImageOver";

	public static final String ELEMENT_IMAGE_CHECKED_OVER = "ImageCheckedOver";

	public static final String ELEMENT_IMAGE_DISABLED = "ImageDisabled";

	public static final String ELEMENT_TEXT = "Text";

	public static final String ELEMENT_TEXTURE = "Texture";

	public static final String ELEMENT_MANAGER = "Manager";

	public static final String ELEMENT_UP = "Up";

	public static final String ELEMENT_DOWN = "Down";

	public static final String ELEMENT_CHECKED = "Checked";

	public static final String ELEMENT_OVER = "Over";

	public static final String ELEMENT_CHECKED_OVER = "CheckedOver";

	public static final String ELEMENT_DISABLED = "Disabled";

	public static final String ELEMENT_PRESSED_OFFSET_X = "PressedOffsetX";

	public static final String ELEMENT_PRESSED_OFFSET_Y = "PressedOffsetY";

	public static final String ELEMENT_UNPRESSED_OFFSET_X = "UnpressedOffsetX";

	public static final String ELEMENT_UNPRESSED_OFFSET_Y = "UnpressedOffsetY";

	public static final String ELEMENT_CHECKED_OFFSET_X = "CheckedOffsetX";

	public static final String ELEMENT_CHECKED_OFFSET_Y = "CheckedOffsetY";

	public static final String ELEMENT_FIELD_OF_VIEW_Y = "FieldOfViewY";

	public static final String ELEMENT_VIEWPORT_WIDTH = "ViewportWidth";

	public static final String ELEMENT_VIEWPORT_HEIGHT = "ViewportHeight";

	public static final String ELEMENT_CHECKBOX_OFF = "CheckBoxOff";

	public static final String ELEMENT_CHECKBOX_ON = "CheckBoxOn";

	public static final String ELEMENT_FONT = "Font";

	public static final String ELEMENT_CURSOR = "Cursor";

	public static final String ELEMENT_VERTEX_SHADER = "VertexShader";

	public static final String ELEMENT_FRAGMENT_SHADER = "FragmentShader";

	public static final String ELEMENT_FONT_COLOR = "FontColor";

	public static final String ELEMENT_BACKGROUND = "Background";

	public static final String ELEMENT_MESSAGE_FONT = "MessageFont";

	public static final String ELEMENT_MESSAGE_FONT_COLOR = "MessageFontColor";

	public static final String ELEMENT_FOCUSED_BACKGROUND = "FocusedBackground";

	public static final String ELEMENT_FOCUSED_FONT_COLOR = "FocusedFontColor";

	public static final String ELEMENT_FIRST_WIDGET = "FirstWidget";

	public static final String ELEMENT_SECOND_WIDGET = "SecondWidget";

	public static final String ELEMENT_HANDLE = "Handle";

	public static final String ELEMENT_BATCH = "Batch";

	public static final String ELEMENT_VIEWPORT = "Viewport";

	public static final String ELEMENT_SCROLL_STYLE = "ScrollStyle";

	public static final String ELEMENT_LIST_STYLE = "ListStyle";

	public static final String ELEMENT_KNOB = "Knob";

	public static final String ELEMENT_DISABLED_BACKGROUND = "DisabledBackground";

	public static final String ELEMENT_DISABLED_KNOB = "DisabledKnob";

	public static final String ELEMENT_KNOB_BEFORE = "KnobBefore";

	public static final String ELEMENT_KNOB_AFTER = "KnobAfter";

	public static final String ELEMENT_DISABLED_KNOB_BEFORE = "DisabledKnobBefore";

	public static final String ELEMENT_DISABLED_KNOB_AFTER = "DisabledKnobAfter";

	public static final String ELEMENT_KNOB_OVER = "KnobOver";

	public static final String ELEMENT_KNOB_DOWN = "KnobDown";

	public static final String ELEMENT_DOWN_FONT_COLOR = "DownFontColor";

	public static final String ELEMENT_OVER_FONT_COLOR = "OverFontColor";

	public static final String ELEMENT_CHECKED_FONT_COLOR = "CheckedFontColor";

	public static final String ELEMENT_CHECKED_OVER_FONT_COLOR = "CheckedOverFontColor";

	public static final String ELEMENT_DISABLED_FONT_COLOR = "DisabledFontColor";

	public static final String ELEMENT_CHECKBOX_OVER = "CheckBoxOver";

	public static final String ELEMENT_CHECKBOX_ON_DISABLED = "CheckBoxOnDisabled";

	public static final String ELEMENT_CHECKBOX_OFF_DISABLED = "CheckBoxOffDisabled";

	public static final String ELEMENT_COLOR = "Color";

	public static final String ELEMENT_TIMELINE = "Timeline";

	public static final String ELEMENT_SCALE = "Scale";

	public static final String ELEMENT_BACKGROUND_DISABLED = "BackgroundDisabled";

	public static final String ELEMENT_BACKGROUND_OPEN = "BackgroundOpen";

	public static final String ELEMENT_BACKGROUND_OVER = "BackgroundOver";

	public static final String ELEMENT_COLOR_NAME = "ColorName";

	public static final String ELEMENT_RGB = "rgb";

	public static final String ELEMENT_ALPHA = "Alpha";

	public static final String ELEMENT_FADE_OUT = "FadeOut";

	public static final String ELEMENT_FADE_IN = "FadeIn";

	public static final String ELEMENT_ACTION = "Action";

	public static final String ELEMENT_TTF = "ttf";

	public static final String ELEMENT_FONT_SCALE = "FontScale";

	public static final String ELEMENT_VISIBLE = "Visible";

	public static final String ELEMENT_USER_OBJECT = "UserObject";

	public static final String ELEMENT_MOVE_BY = "MoveBy";

	public static final String ELEMENT_MOVE_TO = "MoveTo";

	public static final String ELEMENT_MOVE_TO_ALIGNED = "MoveToAligned";

	public static final String ELEMENT_SIZE_TO = "SizeTo";

	public static final String ELEMENT_SIZE_BY = "SizeBy";

	public static final String ELEMENT_SCALE_TO = "ScaleTo";

	public static final String ELEMENT_SCALE_BY = "ScaleBy";

	public static final String ELEMENT_ROTATE_TO = "RotateTo";

	public static final String ELEMENT_ROTATE_BY = "RotateBy";

	public static final String ELEMENT_CONTENT = "Content";

	public static final String ELEMENT_PLUS = "Plus";

	public static final String ELEMENT_MINUS = "Minus";

	public static final String ELEMENT_DEAD_ZONE_RADIUS = "DeadZoneRadius";

	public static final String ELEMENT_CAMERA = "Camera";

	public static final String ELEMENT_WORLD_WIDTH = "WorldWidth";

	public static final String ELEMENT_WORLD_HEIGHT = "WorldHeight";

	public static final String ELEMENT_SCALING = "Scaling";

	public static final String ELEMENT_ALIGN = "Align";

	public static final String ELEMENT_SPEED = "Speed";

	public static final String ELEMENT_REGION = "Region";

	public static final String ELEMENT_SPRITE = "Sprite";

	public static final String ELEMENT_PATCH = "Patch";

	public static final String ELEMENT_MINIMUM_WORLD_HEIGHT = "MinimumWorldHeight";

	public static final String ELEMENT_MINIMUM_WORLD_WIDTH = "MinimumWorldWidth";

	public static final String ELEMENT_MAXIMUM_WORLD_HEIGHT = "MaximumWorldHeight";

	public static final String ELEMENT_MAXIMUM_WORLD_WIDTH = "MaximumWorldWidth";

	public static final String ELEMENT_TITLE_FONT = "TitleFont";

	public static final String ELEMENT_TITLE_FONT_COLOR = "TitleFontColor";

	public static final String ELEMENT_STAGE_BACKGROUND = "StageBackground";

	public static final String ELEMENT_SCREEN = "Screen";

	public static final String ELEMENT_STAGE = "Stage";

	public static final String ELEMENT_TEXTURE_ATLAS = "TextureAtlas";

	public static final String ELEMENT_ARGUMENT = "Argument";

	private static DocumentBuilderFactory FACTORY;

	public static DocumentBuilder BUILDER;

	static {
		FACTORY = DocumentBuilderFactory.newInstance();
		try {
			BUILDER = FACTORY.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			if (Gdx.app != null) {
				Gdx.app.error(TAG, e.getMessage());
			}
		}
		if (Gdx.app != null) {
			Gdx.app.log(TAG, TAG + " initialized");
		}
	}

	public static Object findRef(ArrayMap<String, Object> registry, Element element, Attr ref) {
		if (!registry.containsKey(ref.getValue())) {
			throw new GdxRuntimeException(
					String.format("tag %s - could not find %s in registry ", element.getTagName(), ref.getValue()));
		} else {
			return registry.get(ref.getValue());
		}
	}

	public static void register(ArrayMap<String, Object> registry, Element element, Object object) {
		Attr id = element.getAttributeNode("id");
		if (id != null) {
			register(registry, element.getTagName(), id.getValue(), object);
		}
	}

	protected static void register(ArrayMap<String, Object> registry, String tagName, String id, Object object) {
		if (registry.containsKey(id)) {
			throw new GdxRuntimeException(String.format("tag %s - there is %s in registry already", tagName, id));
		} else {
			registry.put(id, object);
			if (object instanceof TextureAtlas) {
				for (AtlasRegion region : ((TextureAtlas) object).getRegions()) {
					String newId = null;
					if (region.index >= 0) {
						newId = id + "." + region.name + "[" + region.index + "]";
					} else {
						newId = id + "." + region.name;
					}
					register(registry, tagName, newId, region);
				}
			}
		}
	}

}
