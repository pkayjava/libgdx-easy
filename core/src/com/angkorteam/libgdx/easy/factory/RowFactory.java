package com.angkorteam.libgdx.easy.factory;

import org.mariuszgromada.math.mxparser.Argument;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class RowFactory {

	public static void create(Table table, Array<Argument> argument, ArrayMap<String, Object> registry,
			Element element) {
		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);

				Cell<?> cell = null;
				if (XMLFactory.ELEMENT_LABEL.equals(e.getTagName())) {
					cell = table.add(LabelFactory.create(argument, registry, e));
				} else if (XMLFactory.ELEMENT_TEXT_FIELD.equals(e.getTagName())) {
					cell = table.add(TextFieldFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_BUTTON.equals(e.getTagName())) {
					cell = table.add(ButtonFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_TEXT_BUTTON.equals(e.getTagName())) {
					cell = table.add(TextButtonFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_IMAGE_BUTTON.equals(e.getTagName())) {
					cell = table.add(ImageButtonFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_IMAGE_TEXT_BUTTON.equals(e.getTagName())) {
					cell = table.add(ImageTextButtonFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_CHECK_BOX.equals(e.getTagName())) {
					cell = table.add(CheckBoxFactory.create(registry, e));
				} else if (XMLFactory.ELEMENT_IMAGE.equals(e.getTagName())) {
					cell = table.add(ImageFactory.create(argument, registry, e));
				} else if (XMLFactory.ELEMENT_LIST.equals(e.getTagName())) {
					cell = table.add(ImageFactory.create(argument, registry, e));
				} else if (XMLFactory.ELEMENT_SELECT_BOX.equals(e.getTagName())) {
					cell = table.add(ImageFactory.create(argument, registry, e));
				} else {
					throw new GdxRuntimeException(String.format("unknown element %s", e.getTagName()));
				}

				if (e.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN) != null) {
					float margin = Float.valueOf(e.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN).getValue());
					cell.pad(margin);
				}

				if (e.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN_BOTTOM) != null) {
					float marginBottom = Float
							.valueOf(e.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN_BOTTOM).getValue());
					cell.padBottom(marginBottom);
				}

				if (e.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN_LEFT) != null) {
					float marginLeft = Float.valueOf(e.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN_LEFT).getValue());
					cell.padLeft(marginLeft);
				}

				if (e.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN_RIGHT) != null) {
					float marginRight = Float.valueOf(e.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN_RIGHT).getValue());
					cell.padRight(marginRight);
				}

				if (e.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN_TOP) != null) {
					float marginTop = Float.valueOf(e.getAttributeNode(XMLFactory.ATTRIBUTE_MARGIN_TOP).getValue());
					cell.padTop(marginTop);
				}

				if (e.getAttributeNode(XMLFactory.ATTRIBUTE_EXPAND_X) != null) {
					boolean expandX = Boolean.valueOf(e.getAttributeNode(XMLFactory.ATTRIBUTE_EXPAND_X).getValue());
					if (expandX) {
						cell.expandX();
					}
				}

				if (e.getAttributeNode(XMLFactory.ATTRIBUTE_EXPAND_Y) != null) {
					boolean expandY = Boolean.valueOf(e.getAttributeNode(XMLFactory.ATTRIBUTE_EXPAND_Y).getValue());
					if (expandY) {
						cell.expandY();
					}
				}

				if (e.getAttributeNode(XMLFactory.ATTRIBUTE_FILL_X) != null) {
					boolean fillX = Boolean.valueOf(e.getAttributeNode(XMLFactory.ATTRIBUTE_FILL_X).getValue());
					if (fillX) {
						cell.fillX();
					}
				}

				if (e.getAttributeNode(XMLFactory.ATTRIBUTE_FILL_Y) != null) {
					boolean fillY = Boolean.valueOf(e.getAttributeNode(XMLFactory.ATTRIBUTE_FILL_Y).getValue());
					if (fillY) {
						cell.fillY();
					}
				}
			}
		}
	}
}
