package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class TreeStyleFactory {

	public static Tree.TreeStyle create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Tree.TreeStyle) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasPlus = false;
		Drawable plus = null;

		boolean hasMinus = false;
		Drawable minus = null;

		boolean hasSelection = false;
		Drawable selection = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_PLUS.equals(e.getTagName())) {
					hasPlus = true;
					plus = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_MINUS.equals(e.getTagName())) {
					hasMinus = true;
					minus = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_SELECTION.equals(e.getTagName())) {
					hasSelection = true;
					selection = DrawableFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();
		Tree.TreeStyle object = null;
		if (hasPlus && hasMinus && hasSelection) {
			object = new Tree.TreeStyle(plus, minus, selection);
			memory.add(XMLFactory.ELEMENT_PLUS);
			memory.add(XMLFactory.ELEMENT_MINUS);
			memory.add(XMLFactory.ELEMENT_SELECTION);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else if (XMLFactory.ELEMENT_BACKGROUND.equals(e.getTagName())) {
					object.background = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_OVER.equals(e.getTagName())) {
					object.over = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else {
					throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
