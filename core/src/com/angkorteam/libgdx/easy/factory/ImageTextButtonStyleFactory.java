package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class ImageTextButtonStyleFactory {

	public static ImageTextButton.ImageTextButtonStyle create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (ImageTextButton.ImageTextButtonStyle) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasUp = false;
		Drawable up = null;

		boolean hasDown = false;
		Drawable down = null;

		boolean hasChecked = false;
		Drawable checked = null;

		boolean hasFont = false;
		BitmapFont font = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_UP.equals(e.getTagName())) {
					hasUp = true;
					up = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_DOWN.equals(e.getTagName())) {
					hasDown = true;
					down = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_CHECKED.equals(e.getTagName())) {
					hasChecked = true;
					checked = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_FONT.equals(e.getTagName())) {
					hasFont = true;
					font = FontFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();
		ImageTextButton.ImageTextButtonStyle object = null;
		if (hasUp && hasDown && hasChecked && hasFont) {
			object = new ImageTextButton.ImageTextButtonStyle(up, down, checked, font);
			memory.add(XMLFactory.ELEMENT_UP);
			memory.add(XMLFactory.ELEMENT_DOWN);
			memory.add(XMLFactory.ELEMENT_CHECKED);
			memory.add(XMLFactory.ELEMENT_FONT);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else if (XMLFactory.ELEMENT_IMAGE_OVER.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.imageOver = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_IMAGE_CHECKED_OVER.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.imageCheckedOver = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_IMAGE_DISABLED.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.imageDisabled = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_CHECKED_OVER.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.checkedOver = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_DISABLED.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.disabled = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_OVER.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.over = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_PRESSED_OFFSET_X.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.pressedOffsetX = Float.valueOf(e.getTextContent());
				} else if (XMLFactory.ELEMENT_PRESSED_OFFSET_Y.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.pressedOffsetY = Float.valueOf(e.getTextContent());
				} else if (XMLFactory.ELEMENT_UNPRESSED_OFFSET_X.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.unpressedOffsetX = Float.valueOf(e.getTextContent());
				} else if (XMLFactory.ELEMENT_UNPRESSED_OFFSET_Y.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.unpressedOffsetY = Float.valueOf(e.getTextContent());
				} else if (XMLFactory.ELEMENT_CHECKED_OFFSET_Y.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.checkedOffsetY = Float.valueOf(e.getTextContent());
				} else if (XMLFactory.ELEMENT_CHECKED_OFFSET_X.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.checkedOffsetX = Float.valueOf(e.getTextContent());
				} else if (XMLFactory.ELEMENT_DOWN_FONT_COLOR.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.downFontColor = ColorFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_FONT_COLOR.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.fontColor = ColorFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_DISABLED_FONT_COLOR.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.disabledFontColor = ColorFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_CHECKED_OVER_FONT_COLOR.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.checkedOverFontColor = ColorFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_CHECKED_FONT_COLOR.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.checkedFontColor = ColorFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_IMAGE_CHECKED.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.imageChecked = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_IMAGE_DOWN.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.imageDown = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_IMAGE_UP.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.imageUp = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_OVER_FONT_COLOR.equals(e.getTagName())) {
					memory.add(e.getTagName());
					object.overFontColor = ColorFactory.create(registry, e);
				} else {
					throw new GdxRuntimeException(String.format("constructor %s invalid", e.getTagName()));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
