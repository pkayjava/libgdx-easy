package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class ColorFactory {

	public static Color create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);
		Attr name = element.getAttributeNode(XMLFactory.ATTRIBUTE_NAME);

		Array<Attr> attr = new Array<Attr>(3);
		if (id != null) {
			attr.add(id);
		}
		if (ref != null) {
			attr.add(ref);
		}
		if (name != null) {
			attr.add(name);
		}

		if (attr.size > 1) {
			throw new GdxRuntimeException(String.format("element %s attribute id, ref and name are conflict", tagName));
		}

		if (ref != null) {
			return (Color) XMLFactory.findRef(registry, element, ref);
		}

		if (name != null) {
			if (XMLFactory.ATTRIBUTE_NAME_CLEAR.equalsIgnoreCase(name.getValue())) {
				return Color.CLEAR;
			} else if (XMLFactory.ATTRIBUTE_NAME_BLACK.equalsIgnoreCase(name.getValue())) {
				return Color.BLACK;
			} else if (XMLFactory.ATTRIBUTE_NAME_MAROON.equalsIgnoreCase(name.getValue())) {
				return Color.MAROON;
			} else if (XMLFactory.ATTRIBUTE_NAME_VIOLET.equalsIgnoreCase(name.getValue())) {
				return Color.VIOLET;
			} else if (XMLFactory.ATTRIBUTE_NAME_PURPLE.equalsIgnoreCase(name.getValue())) {
				return Color.PURPLE;
			} else if (XMLFactory.ATTRIBUTE_NAME_MAGENTA.equalsIgnoreCase(name.getValue())) {
				return Color.MAGENTA;
			} else if (XMLFactory.ATTRIBUTE_NAME_PINK.equalsIgnoreCase(name.getValue())) {
				return Color.PINK;
			} else if (XMLFactory.ATTRIBUTE_NAME_SALMON.equalsIgnoreCase(name.getValue())) {
				return Color.SALMON;
			} else if (XMLFactory.ATTRIBUTE_NAME_CORAL.equalsIgnoreCase(name.getValue())) {
				return Color.CORAL;
			} else if (XMLFactory.ATTRIBUTE_NAME_SCARLET.equalsIgnoreCase(name.getValue())) {
				return Color.SCARLET;
			} else if (XMLFactory.ATTRIBUTE_NAME_RED.equalsIgnoreCase(name.getValue())) {
				return Color.RED;
			} else if (XMLFactory.ATTRIBUTE_NAME_FIREBRICK.equalsIgnoreCase(name.getValue())) {
				return Color.FIREBRICK;
			} else if (XMLFactory.ATTRIBUTE_NAME_TAN.equalsIgnoreCase(name.getValue())) {
				return Color.TAN;
			} else if (XMLFactory.ATTRIBUTE_NAME_BROWN.equalsIgnoreCase(name.getValue())) {
				return Color.BROWN;
			} else if (XMLFactory.ATTRIBUTE_NAME_ORANGE.equalsIgnoreCase(name.getValue())) {
				return Color.ORANGE;
			} else if (XMLFactory.ATTRIBUTE_NAME_GOLDENROD.equalsIgnoreCase(name.getValue())) {
				return Color.GOLDENROD;
			} else if (XMLFactory.ATTRIBUTE_NAME_GOLD.equalsIgnoreCase(name.getValue())) {
				return Color.GOLD;
			} else if (XMLFactory.ATTRIBUTE_NAME_YELLOW.equalsIgnoreCase(name.getValue())) {
				return Color.YELLOW;
			} else if (XMLFactory.ATTRIBUTE_NAME_OLIVE.equalsIgnoreCase(name.getValue())) {
				return Color.OLIVE;
			} else if (XMLFactory.ATTRIBUTE_NAME_FOREST.equalsIgnoreCase(name.getValue())) {
				return Color.FOREST;
			} else if (XMLFactory.ATTRIBUTE_NAME_LIME.equalsIgnoreCase(name.getValue())) {
				return Color.LIME;
			} else if (XMLFactory.ATTRIBUTE_NAME_CHARTREUSE.equalsIgnoreCase(name.getValue())) {
				return Color.CHARTREUSE;
			} else if (XMLFactory.ATTRIBUTE_NAME_GREEN.equalsIgnoreCase(name.getValue())) {
				return Color.GREEN;
			} else if (XMLFactory.ATTRIBUTE_NAME_TEAL.equalsIgnoreCase(name.getValue())) {
				return Color.TEAL;
			} else if (XMLFactory.ATTRIBUTE_NAME_CYAN.equalsIgnoreCase(name.getValue())) {
				return Color.CYAN;
			} else if (XMLFactory.ATTRIBUTE_NAME_SKY.equalsIgnoreCase(name.getValue())) {
				return Color.SKY;
			} else if (XMLFactory.ATTRIBUTE_NAME_SLATE.equalsIgnoreCase(name.getValue())) {
				return Color.SLATE;
			} else if (XMLFactory.ATTRIBUTE_NAME_ROYAL.equalsIgnoreCase(name.getValue())) {
				return Color.ROYAL;
			} else if (XMLFactory.ATTRIBUTE_NAME_NAVY.equalsIgnoreCase(name.getValue())) {
				return Color.NAVY;
			} else if (XMLFactory.ATTRIBUTE_NAME_BLACK.equalsIgnoreCase(name.getValue())) {
				return Color.BLUE;
			} else if (XMLFactory.ATTRIBUTE_NAME_DARK_GRAY.equalsIgnoreCase(name.getValue())) {
				return Color.DARK_GRAY;
			} else if (XMLFactory.ATTRIBUTE_NAME_GRAY.equalsIgnoreCase(name.getValue())) {
				return Color.GRAY;
			} else if (XMLFactory.ATTRIBUTE_NAME_LIGHT_GRAY.equalsIgnoreCase(name.getValue())) {
				return Color.LIGHT_GRAY;
			} else if (XMLFactory.ATTRIBUTE_NAME_WHITE.equalsIgnoreCase(name.getValue())) {
				return Color.WHITE;
			} else {
				throw new GdxRuntimeException(String.format("Unknown color name %s", name.getValue()));
			}
		}

		// constructor section

		boolean hasRgb = false;
		float r = -1;
		float g = -1;
		float b = -1;

		boolean hasAlpha = false;
		float alpha = -1f;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_RGB.equals(e.getTagName())) {
					hasRgb = true;
					String rgb = e.getTextContent().startsWith("#") ? e.getTextContent().substring(1)
							: e.getTextContent();
					r = Integer.parseInt(rgb.substring(0, 2), 16) / 255f;
					g = Integer.parseInt(rgb.substring(2, 4), 16) / 255f;
					b = Integer.parseInt(rgb.substring(4), 16) / 255f;
				} else if (XMLFactory.ELEMENT_ALPHA.equals(e.getTagName())) {
					hasAlpha = true;
					alpha = Float.valueOf(e.getTextContent());
				}
			}
		}

		Color object = null;
		if (hasRgb && hasAlpha) {
			object = new Color(r, g, b, alpha);
		} else if (hasRgb) {
			object = new Color(r, g, b, 1f);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
