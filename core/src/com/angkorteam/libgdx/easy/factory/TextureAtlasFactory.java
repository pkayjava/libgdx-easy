package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class TextureAtlasFactory {

	private static String TAG = TextureAtlasFactory.class.getSimpleName();

	public static TextureAtlas create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (TextureAtlas) XMLFactory.findRef(registry, element, ref);
		}

		// constructor section

		String path = element.getTextContent().trim();
		TextureAtlas object = new TextureAtlas(path);

		for (AtlasRegion region : object.getRegions()) {
			if (id != null) {
				if (region.index >= 0) {
					Gdx.app.log(TAG,
							String.format("path %s width %d height %d",
									path + " :: " + id.getValue() + "." + region.name + "[" + region.index + "]",
									region.getRegionWidth(), region.getRegionHeight()));
				} else {
					Gdx.app.log(TAG,
							String.format("path %s width %d height %d",
									path + " :: " + id.getValue() + "." + region.name, region.getRegionWidth(),
									region.getRegionHeight()));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
