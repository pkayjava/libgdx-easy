package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class FontFactory {

	public static BitmapFont create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (BitmapFont) XMLFactory.findRef(registry, element, ref);
		}

		// constructor section

		boolean hasSize = false;
		int size = -1;

		boolean hasTtf = false;
		String ttf = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_TTF.equals(e.getTagName())) {
					hasTtf = true;
					ttf = e.getTextContent();
				} else if (XMLFactory.ELEMENT_SIZE.equals(e.getTagName())) {
					hasSize = true;
					size = Integer.valueOf(e.getTextContent().trim());
				}
			}
		}

		BitmapFont object = null;

		if (hasSize && hasTtf) {
			FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(ttf));
			FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
			parameter.size = size;
			object = generator.generateFont(parameter);
			generator.dispose();
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		XMLFactory.register(registry, element, object);

		return object;

	}

}
