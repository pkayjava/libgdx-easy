package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.angkorteam.libgdx.easy.validation.InterpolationValidator;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class MoveToActionFactory {

	public static MoveToAction create(ArrayMap<String, Object> registry, Element element) {
		Attr x = element.getAttributeNode(XMLFactory.ATTRIBUTE_X);
		Attr y = element.getAttributeNode(XMLFactory.ATTRIBUTE_Y);
		Attr duration = element.getAttributeNode(XMLFactory.ATTRIBUTE_DURATION);
		Attr interpolation = element.getAttributeNode(XMLFactory.ATTRIBUTE_INTERPOLATION);
		if (x != null && y != null && duration != null && interpolation != null) {
			return Actions.moveTo(Float.valueOf(x.getValue()), Float.valueOf(y.getValue()),
					Float.valueOf(duration.getValue()),
					InterpolationValidator.parse(interpolation.getTextContent().trim()));
		} else if (x != null && y != null && duration != null) {
			return Actions.moveTo(Float.valueOf(x.getValue()), Float.valueOf(y.getValue()),
					Float.valueOf(duration.getValue()));
		} else if (x != null && y != null) {
			return Actions.moveTo(Float.valueOf(x.getValue()), Float.valueOf(y.getValue()));
		} else {
			throw new GdxRuntimeException(String.format("%s invalid", element.getTagName()));
		}
	}

}
