package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class TextFieldStyleFactory {

	public static TextField.TextFieldStyle create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (TextField.TextFieldStyle) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasFontColor = false;
		Color fontColor = null;

		boolean hasFont = false;
		BitmapFont font = null;

		boolean hasCursor = false;
		Drawable cursor = null;

		boolean hasSelection = false;
		Drawable selection = null;

		boolean hasBackground = false;
		Drawable background = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_FONT_COLOR.equals(e.getTagName())) {
					hasFontColor = true;
					fontColor = ColorFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_FONT.equals(e.getTagName())) {
					hasFont = true;
					font = FontFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_CURSOR.equals(e.getTagName())) {
					hasCursor = true;
					cursor = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_SELECTION.equals(e.getTagName())) {
					hasSelection = true;
					selection = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_BACKGROUND.equals(e.getTagName())) {
					hasBackground = true;
					background = DrawableFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();
		TextField.TextFieldStyle object = null;
		if (hasFont && hasFontColor && hasCursor && hasSelection && hasBackground) {
			object = new TextField.TextFieldStyle(font, fontColor, cursor, selection, background);
			memory.add(XMLFactory.ELEMENT_FONT);
			memory.add(XMLFactory.ELEMENT_FONT_COLOR);
			memory.add(XMLFactory.ELEMENT_CURSOR);
			memory.add(XMLFactory.ELEMENT_SELECTION);
			memory.add(XMLFactory.ELEMENT_BACKGROUND);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else if (XMLFactory.ELEMENT_FOCUSED_BACKGROUND.equals(e.getTagName())) {
					object.focusedBackground = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_DISABLED_BACKGROUND.equals(e.getTagName())) {
					object.disabledBackground = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_FOCUSED_FONT_COLOR.equals(e.getTagName())) {
					object.focusedFontColor = ColorFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_DISABLED_FONT_COLOR.equals(e.getTagName())) {
					object.disabledFontColor = ColorFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_MESSAGE_FONT.equals(e.getTagName())) {
					object.messageFont = FontFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_MESSAGE_FONT_COLOR.equals(e.getTagName())) {
					object.messageFontColor = ColorFactory.create(registry, e);
					memory.add(e.getTagName());
				} else {
					throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
