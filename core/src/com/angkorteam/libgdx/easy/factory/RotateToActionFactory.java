package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.angkorteam.libgdx.easy.validation.InterpolationValidator;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class RotateToActionFactory {

	public static RotateToAction create(ArrayMap<String, Object> registry, Element element) {
		Attr rotation = element.getAttributeNode(XMLFactory.ATTRIBUTE_ROTATION);
		Attr duration = element.getAttributeNode(XMLFactory.ATTRIBUTE_DURATION);
		Attr interpolation = element.getAttributeNode(XMLFactory.ATTRIBUTE_INTERPOLATION);
		if (rotation != null && duration != null && interpolation != null) {
			return Actions.rotateTo(Float.valueOf(rotation.getValue()), Float.valueOf(duration.getValue()),
					InterpolationValidator.parse(interpolation.getTextContent().trim()));
		} else if (rotation != null && duration != null) {
			return Actions.rotateTo(Float.valueOf(rotation.getValue()), Float.valueOf(duration.getValue()));
		} else if (rotation != null) {
			return Actions.rotateTo(Float.valueOf(rotation.getValue()));
		} else {
			throw new GdxRuntimeException(String.format("%s invalid", element.getTagName()));
		}
	}

}
