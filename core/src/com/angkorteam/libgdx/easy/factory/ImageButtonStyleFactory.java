package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class ImageButtonStyleFactory {

	public static ImageButton.ImageButtonStyle create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (ImageButton.ImageButtonStyle) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasUp = false;
		Drawable up = null;

		boolean hasDown = false;
		Drawable down = null;

		boolean hasChecked = false;
		Drawable checked = null;

		boolean hasImageUp = false;
		Drawable imageUp = null;

		boolean hasImageDown = false;
		Drawable imageDown = null;

		boolean hasImageChecked = false;
		Drawable imageChecked = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_UP.equals(e.getTagName())) {
					hasUp = true;
					up = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_DOWN.equals(e.getTagName())) {
					hasDown = true;
					down = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_CHECKED.equals(e.getTagName())) {
					hasChecked = true;
					checked = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_IMAGE_UP.equals(e.getTagName())) {
					hasImageUp = true;
					imageUp = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_IMAGE_DOWN.equals(e.getTagName())) {
					hasImageDown = true;
					imageDown = DrawableFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_IMAGE_CHECKED.equals(e.getTagName())) {
					hasImageChecked = true;
					imageChecked = DrawableFactory.create(registry, e);
				}
			}
		}

		Array<String> memory = new Array<String>();
		ImageButton.ImageButtonStyle object = null;
		if (hasUp && hasDown && hasChecked && hasImageUp && hasImageDown && hasImageChecked) {
			object = new ImageButton.ImageButtonStyle(up, down, checked, imageUp, imageDown, imageChecked);
			memory.add(XMLFactory.ELEMENT_UP);
			memory.add(XMLFactory.ELEMENT_DOWN);
			memory.add(XMLFactory.ELEMENT_CHECKED);
			memory.add(XMLFactory.ELEMENT_IMAGE_UP);
			memory.add(XMLFactory.ELEMENT_IMAGE_DOWN);
			memory.add(XMLFactory.ELEMENT_IMAGE_CHECKED);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else if (XMLFactory.ELEMENT_IMAGE_OVER.equals(e.getTagName())) {
					object.imageOver = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_IMAGE_CHECKED_OVER.equals(e.getTagName())) {
					object.imageCheckedOver = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_IMAGE_DISABLED.equals(e.getTagName())) {
					object.imageDisabled = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_CHECKED.equals(e.getTagName())) {
					object.checked = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_CHECKED_OVER.equals(e.getTagName())) {
					object.checkedOver = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_DISABLED.equals(e.getTagName())) {
					object.disabled = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_OVER.equals(e.getTagName())) {
					object.over = DrawableFactory.create(registry, e);
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_PRESSED_OFFSET_X.equals(e.getTagName())) {
					object.pressedOffsetX = Float.valueOf(e.getTextContent());
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_PRESSED_OFFSET_Y.equals(e.getTagName())) {
					object.pressedOffsetY = Float.valueOf(e.getTextContent());
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_UNPRESSED_OFFSET_X.equals(e.getTagName())) {
					object.unpressedOffsetX = Float.valueOf(e.getTextContent());
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_UNPRESSED_OFFSET_Y.equals(e.getTagName())) {
					object.unpressedOffsetY = Float.valueOf(e.getTextContent());
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_CHECKED_OFFSET_Y.equals(e.getTagName())) {
					object.checkedOffsetY = Float.valueOf(e.getTextContent());
					memory.add(e.getTagName());
				} else if (XMLFactory.ELEMENT_CHECKED_OFFSET_X.equals(e.getTagName())) {
					object.checkedOffsetX = Float.valueOf(e.getTextContent());
					memory.add(e.getTagName());
				} else {
					throw new GdxRuntimeException(String.format("constructor %s invalid", e.getTagName()));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
