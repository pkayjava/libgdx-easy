package com.angkorteam.libgdx.easy.factory;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class DialogFactory {

	public static Dialog create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Dialog) XMLFactory.findRef(registry, element, ref);
		}

		boolean hasTitle = false;
		String title = null;

		boolean hasSkin = false;
		Skin skin = null;

		boolean hasStyleName = false;
		String styleName = null;

		boolean hasStyle = false;
		Window.WindowStyle style = null;

		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (XMLFactory.ELEMENT_TITLE.equals(e.getTagName())) {
					hasTitle = true;
					title = e.getTextContent().trim();
				} else if (XMLFactory.ELEMENT_SKIN.equals(e.getTagName())) {
					hasSkin = true;
					skin = SkinFactory.create(registry, e);
				} else if (XMLFactory.ELEMENT_STYLE_NAME.equals(e.getTagName())) {
					hasStyleName = true;
					styleName = e.getTextContent().trim();
				} else if (XMLFactory.ELEMENT_STYLE.equals(e.getTagName())) {
					hasStyle = true;
					style = WindowStyleFactory.create(registry, e);
				}
			}
		}

		Dialog object = null;
		Array<String> memory = new Array<String>();

		if (hasTitle && hasSkin && hasStyleName) {
			object = new Dialog(title, skin, styleName);
			memory.add(XMLFactory.ELEMENT_TITLE);
			memory.add(XMLFactory.ELEMENT_SKIN);
			memory.add(XMLFactory.ELEMENT_STYLE_NAME);
		} else if (hasTitle && hasSkin) {
			object = new Dialog(title, skin);
			memory.add(XMLFactory.ELEMENT_TITLE);
			memory.add(XMLFactory.ELEMENT_SKIN);
		} else if (hasTitle && hasStyle) {
			object = new Dialog(title, style);
			memory.add(XMLFactory.ELEMENT_TITLE);
			memory.add(XMLFactory.ELEMENT_STYLE);
		} else {
			throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
		}

		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) nodes.item(i);
				if (memory.contains(e.getTagName(), false)) {
					continue;
				} else {
					throw new GdxRuntimeException(String.format("constructor %s invalid", tagName));
				}
			}
		}

		XMLFactory.register(registry, element, object);

		return object;
	}

}
