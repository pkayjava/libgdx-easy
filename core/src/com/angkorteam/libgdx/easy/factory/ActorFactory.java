package com.angkorteam.libgdx.easy.factory;

import org.mariuszgromada.math.mxparser.Argument;
import org.mariuszgromada.math.mxparser.Expression;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;

import com.angkorteam.libgdx.easy.validation.AlignmentValidator;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class ActorFactory {

	public static Actor create(ArrayMap<String, Object> registry, Element element) {
		String tagName = element.getTagName();
		Attr id = element.getAttributeNode(XMLFactory.ATTRIBUTE_ID);
		Attr ref = element.getAttributeNode(XMLFactory.ATTRIBUTE_REF);

		if (id != null && ref != null) {
			throw new GdxRuntimeException(String.format("element %s attribute id and ref are conflict", tagName));
		}

		if (ref != null) {
			return (Actor) XMLFactory.findRef(registry, element, ref);
		}

		// constructor section

		Actor object = new Actor();

		XMLFactory.register(registry, element, object);

		return object;
	}

	public static void setPosition(Array<Argument> argument, Element element, Actor object) {
		Attr x = element.getAttributeNode(XMLFactory.ATTRIBUTE_X);
		Attr y = element.getAttributeNode(XMLFactory.ATTRIBUTE_Y);
		Attr alignment = element.getAttributeNode(XMLFactory.ATTRIBUTE_ALIGNMENT);
		if (x != null && y != null && alignment != null) {
			int a = Integer.valueOf(alignment.getValue());
			Expression x1 = new Expression(x.getValue());
			x1.addArguments(argument.toArray());
			Expression y1 = new Expression(y.getValue());
			y1.addArguments(argument.toArray());
			object.setPosition(Double.valueOf(x1.calculate()).floatValue(), Double.valueOf(y1.calculate()).floatValue(),
					AlignmentValidator.parse(a));
		} else if (x != null && y != null) {
			Expression x1 = new Expression(x.getValue());
			x1.addArguments(argument.toArray());
			Expression y1 = new Expression(y.getValue());
			y1.addArguments(argument.toArray());
			object.setPosition(Double.valueOf(x1.calculate()).floatValue(),
					Double.valueOf(y1.calculate()).floatValue());
		} else {
			throw new GdxRuntimeException(String.format("invalid %s", element.getTagName()));
		}
	}

	public static void setSize(Element element, Actor object) {
		Attr width = element.getAttributeNode(XMLFactory.ATTRIBUTE_WIDTH);
		Attr height = element.getAttributeNode(XMLFactory.ATTRIBUTE_HEIGHT);
		if (width != null && height != null) {
			object.setSize(Float.valueOf(width.getValue()), Float.valueOf(height.getValue()));
		} else {
			throw new GdxRuntimeException(String.format("invalid %s", element.getTagName()));
		}
	}

	public static void setColor(ArrayMap<String, Object> registry, Element element, Actor object) {
		Color color = ColorFactory.create(registry, element);
		object.setColor(color);
	}

	public static void setAction(ArrayMap<String, Object> registry, Element element, Actor object) {
		object.addAction(TimelineActionFactory.create(registry, element));
	}

	public static void setScale(Element element, Actor object) {
		Attr width = element.getAttributeNode(XMLFactory.ATTRIBUTE_WIDTH);
		Attr height = element.getAttributeNode(XMLFactory.ATTRIBUTE_HEIGHT);
		if (width != null && height != null) {
			object.setScale(Float.valueOf(width.getValue()), Float.valueOf(height.getValue()));
		} else {
			throw new GdxRuntimeException(String.format("invalid %s", element.getTagName()));
		}
	}

}
