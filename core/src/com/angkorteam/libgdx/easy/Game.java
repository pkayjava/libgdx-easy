package com.angkorteam.libgdx.easy;

import java.io.IOException;
import java.io.InputStream;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.angkorteam.libgdx.easy.factory.BatchFactory;
import com.angkorteam.libgdx.easy.factory.CameraFactory;
import com.angkorteam.libgdx.easy.factory.ColorFactory;
import com.angkorteam.libgdx.easy.factory.DrawableFactory;
import com.angkorteam.libgdx.easy.factory.FontFactory;
import com.angkorteam.libgdx.easy.factory.LabelStyleFactory;
import com.angkorteam.libgdx.easy.factory.ShaderProgramFactory;
import com.angkorteam.libgdx.easy.factory.SkinFactory;
import com.angkorteam.libgdx.easy.factory.TextFieldFactory;
import com.angkorteam.libgdx.easy.factory.ViewportFactory;
import com.angkorteam.libgdx.easy.factory.XMLFactory;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.GdxRuntimeException;

public abstract class Game extends com.badlogic.gdx.Game {

	private static final String TAG = Game.class.getSimpleName();

	private ArrayMap<String, Object> registry;

	@Override
	public void create() {
		String clazz = getClass().getSimpleName();
		FileHandle handle = Gdx.files.internal("xml/" + clazz + ".xml");

		this.registry = new ArrayMap<>();

		try (InputStream inputStream = handle.read()) {
			Document document = XMLFactory.BUILDER.parse(inputStream);
			Element rootElement = document.getDocumentElement();
			String tagName = rootElement.getTagName();
			if ("Game".equals(tagName)) {
				NodeList nodes = rootElement.getChildNodes();
				for (int i = 0; i < nodes.getLength(); i++) {
					if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
						Element e = (Element) nodes.item(i);
						if ("ShaderProgram".equals(e.getTagName())) {
							ShaderProgramFactory.create(this.registry, e);
						} else if ("Skin".equals(e.getTagName())) {
							SkinFactory.create(this.registry, e);
						} else if ("Camera".equals(e.getTagName())) {
							CameraFactory.create(this.registry, e);
						} else if ("Viewport".equals(e.getTagName())) {
							ViewportFactory.create(this.registry, e);
						} else if ("Batch".equals(e.getTagName())) {
							BatchFactory.create(this.registry, e);
						} else if ("Font".equals(e.getTagName())) {
							FontFactory.create(this.registry, e);
						} else if ("Color".equals(e.getTagName())) {
							ColorFactory.create(this.registry, e);
						} else if ("LabelStyle".equals(e.getTagName())) {
							LabelStyleFactory.create(this.registry, e);
						} else if ("TextFieldStyle".equals(e.getTagName())) {
							TextFieldFactory.create(this.registry, e);
						} else if ("Drawable".equals(e.getTagName())) {
							DrawableFactory.create(this.registry, e);
						} else {
							throw new GdxRuntimeException(String.format("unknown tag name %s", e.getTagName()));
						}
					}
				}
			} else {
				throw new GdxRuntimeException(String.format("unknown tag name %s", tagName));
			}
		} catch (IOException | SAXException e) {
			throw new GdxRuntimeException(e);
		}

	}

	public final ArrayMap<String, Object> getRegistry() {
		return registry;
	}

}
