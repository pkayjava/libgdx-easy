package com.angkorteam.libgdx.easy.example;

import com.angkorteam.libgdx.easy.Game;
import com.angkorteam.libgdx.easy.Screen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

public class SplashScreenThree extends Screen {

	private long time = 0;

	public SplashScreenThree(Game game) {
		super(game);
	}

	@Override
	public void show() {
		super.show();
		this.time = System.currentTimeMillis();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		super.render(delta);
		long newTime = System.currentTimeMillis();
		if (newTime - time >= 2000) {
			this.game.setScreen(new WelcomeScreen(this.game));
		}
	}

}
