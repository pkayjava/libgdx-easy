package com.angkorteam.libgdx.easy.example;

import com.angkorteam.libgdx.easy.Game;
import com.badlogic.gdx.Screen;

public class TestGame extends Game {

	@Override
	public void create() {
		super.create();
		Screen screen = null;
//		screen= new FontScreen(this);
//		screen = new LabelScreen(this);
//		screen = new SplashScreenOne(this);
		screen = new WelcomeScreen(this);
//		screen = new AbcScreen();

		setScreen(screen);
	}

}
