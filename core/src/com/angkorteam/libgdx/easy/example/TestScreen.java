package com.angkorteam.libgdx.easy.example;

import com.angkorteam.libgdx.easy.Game;
import com.angkorteam.libgdx.easy.Screen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

public class TestScreen extends Screen {

	public TestScreen(Game game) {
		super(game);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		super.render(delta);
	}
}
