package com.angkorteam.libgdx.easy.desktop;

import com.angkorteam.libgdx.easy.example.TestGame;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
//		config.width = 360;
//		config.height = 640;
		config.width = 640;
		config.height = 360;
		config.resizable = false;
		config.title = "Proof of Concept";
		new LwjglApplication(new TestGame(), config);
	}
}
